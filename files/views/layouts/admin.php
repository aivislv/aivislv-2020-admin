<?php

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\web\View;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

/* @var $this View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>

<body class="gray-bg">
<?php $this->beginBody() ?>
<div class="content wrapperv">
<?
    $list = [
        'index' => 'Dashboard',
        'users' => 'Users',
        'photography' => [
            'title' => 'Photography',
            'sub' => [
                'albums' => 'Albums',
                'sessions' => 'Sessions',
                'photos' => 'Photos',
                'songs' => 'Songs',
            ]
        ],
        'library' => [
            'title' => 'Library',
            'sub' => [
                'books' => 'Books',
                'authors' => 'Authors',
                'series' => 'Series',
                'books?wish' => 'Wish To..',
            ]
        ],
        'gamelibrary' => [
            'title' => 'Game Library',
            'sub' => [
                'games' => 'Games',
                'gametags' => 'Game tags',
                'games?wish' => 'Wish To..',
                'gametagcats' => 'Game tag categories',
                'newgameexp' => 'New game expansions',
                'gameprices' => 'Game prices'
            ]
        ],
        'animelibrary' => [
            'title' => 'Anime Library',
            'sub' => [
                'animes' => 'Animes',
                'animes?wish' => 'Wish To..',
            ]
        ],
        'movies' => 'Movies',
        'content' => 'Content',
        'crons' => 'Imports',
    ];

$children = Yii::$app->controller->renderPartial('adminNavChildren');

\app\widgets\NavBar::render($list, '/', $children);

?>

    <div class="wrapper wrapper-content animated fadeInRight"
         style="width:100%; max-width: 1600px; box-sizing:border-box; position: relative; margin: 0 auto; margin-top: 10px; padding: 0 15px;">
        <?= Yii::$app->breadcrumbs->render(); ?>
        <?= $content ?>
    </div>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
