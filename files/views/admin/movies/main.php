<h1>Movies</h1>
   <!-- <a class="btn btn-w-m btn-primary" href="/games?new">Add New Game</a> -->
<?
use \yii\data\ActiveDataProvider;
use \yii\grid\GridView;
use \yii\grid\ActionColumn;
use \yii\helpers\Html;

if ($_GET['search']) {
    $query = \app\models\Movies::find()->where(['like', 'title', $_GET['search']] )->orderBy(['title' => SORT_ASC]);
} else {
    $query = \app\models\Movies::find()->orderBy(['title' => SORT_ASC]);
}

$provider = new ActiveDataProvider([
    'query' => $query,
    'pagination' => [
        'pageSize' => 50,
    ]
]);

$provider = new ActiveDataProvider([
    'query' => $query,
]);

?>

<div>
    <div>
        <form method="get" action="/movies">
            <div class="input-group mb-3" style="max-width: 300px;">
                <input placeholder="Search..." class="form-control"  name="search" value="<?=$_GET['search'];?>" />
                <div class="input-group-append">
                    <button type="submit" class="btn btn-outline-primary" >Search</button>
                </div>
            </div>
        </form>
    </div>
    <? if ($_GET['search']) { ?>
    <div class="alert alert-success" role="alert">
        Search results for: <strong><?=$_GET['search'];?></strong>
    </div>
    <? } ?>
    <br />
    <?php
    echo \app\widgets\LinkPagerB4::widget([
        'pagination' => $provider->pagination,
    ]);
    ?>
</div>
<?
echo  \app\widgets\GridViewB4::widget([
    'dataProvider' => $provider,
    'layout' => "{summary}\n{items}",
    'columns' => [
        'id'=>['label'=>'Id','attribute'=>'id'],
        'title'=>['label'=>'Title','attribute'=>'title'],
        'preview'=>['label'=>'', 'format'=>'raw', 'value'=>function($game) {
            return '<img src="https://cdn.aivis.lv/gallery/movies/' . $game->id . '.jpg" />';
        }],
        'owned'=>['label'=>'Status', 'format'=>'raw', 'value'=>function($game) {
            $status = [
                '1' => 'Watched',
                '0' => 'Wishlist',
            ];

            return $status[$game->status];
        }],
        'buttons'=>[
            'class' => ActionColumn::className(),
            'template'=>('{update} {delete}'),
            'buttons'=>[
                'update' => function ($url, $model, $key) {return Html::a('<span class="fa fa-edit fa-fw"></span>', '/movies/' . $model->id, ['class'=>'btn  btn-primary btn-xs']);},
                'delete' => function ($url, $model, $key) {return Html::a('<span class="fa fa-trash fa-fw"></span>', '/movies/' . $model->id . "?delete", ['onclick'=>'return confirm("Really delete?");','class'=>'btn  btn-danger btn-xs']);},
            ],
        ]
    ],
]) ?>
