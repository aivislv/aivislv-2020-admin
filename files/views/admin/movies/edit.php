<?

$formInputs = [
    'photo'=>['title'=>'Anime image', 'source' => 'gallery/anime/' . $item->id . '.jpg', 'type'=> 'showImage'],
    'title'=>['title'=>'Title'],
    'imdb_id'=>['title'=>'IMDB id', 'type'=>'disabled'],
    'rating'=>['title'=>'Rating', 'type'=>'disabled'],
    'score'=>['title'=>'Score', 'type'=>'disabled'],
    'status' => ['title'=>'Type', 'type'=>'selectSimple','data'=>[
        '1' => 'Watched',
        '0' => 'Wishlist',
        ]],
    'description'=>['title'=>'Description', 'type'=>'echo', 'text'=> $item->description],
];

echo $this->render('@app/views/admin/blank/edit', array('item'=>$item,'post'=>'movies', 'formInputs'=>$formInputs));