<h1>Songs</h1>
<a class="btn btn-w-m btn-primary" href="/songs?new">Add New Song</a>
<br/><br/>
<?
use \yii\data\ActiveDataProvider;
use \yii\grid\GridView;
use \yii\grid\ActionColumn;
use \yii\helpers\Html;

$query = \app\models\Songs::find();

$provider = new ActiveDataProvider([
    'query' => $query,
]);

echo  \app\widgets\GridViewB4::widget([
    'dataProvider' => $provider,
    'columns' => [
        'id'=>['label'=>'Id','attribute'=>'id'],
        'title'=>['label'=>'Title','attribute'=>'title'],
        'artist'=>['label'=>'Artist','attribute'=>'artist'],
        'visible'=>['label'=>'Visible', 'format'=>'raw', 'value'=>function($song) {
            return $song->visible == 1 ? '<span style="color: #1e7e34" class="fa fa-eye fa-fw"></span>' : '<span style="color: #a91c19" class="fa fa-eye fa-fw"></span>';
        }],
        'buttons'=>[
            'class' => ActionColumn::className(),
            'template'=>('{update} {delete}'),
            'buttons'=>[
                'update' => function ($url, $model, $key) {return Html::a('<span class="fa fa-edit fa-fw"></span>', '/songs/' . $model->id, ['class'=>'btn  btn-primary btn-xs']);},
                'delete' => function ($url, $model, $key) {return Html::a('<span class="fa fa-trash fa-fw"></span>', '/songs/' . $model->id . "?delete", ['onclick'=>'return confirm("Really delete?");','class'=>'btn  btn-danger btn-xs']);},
            ],
        ]
    ],
]) ?>
