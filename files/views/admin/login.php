<form class="form-signin" method="post" action="?login">
    <div class="form-objects">
    <h2 class="form-signin-heading">Please login!</h2>
    <label for="inputEmail" class="sr-only">Username</label>
    <input type="text" id="inputEmail" name="username" class="form-control spacing-bottom " placeholder="User" required="" autofocus="">
    <label for="inputPassword" class="sr-only">Password</label>
    <input type="password" id="inputPassword" name="password" class="form-control spacing-bottom " placeholder="Parole" required="">
    <input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>" />
    <button class="btn btn-lg btn-primary btn-block" type="submit">Login</button>
    </div>
</form>