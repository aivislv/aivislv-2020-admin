<h1>Books Series</h1>
<a class="btn btn-w-m btn-primary" href="/series?new">Add New Series</a>
<br/><br/>
<!-- <a class="btn btn-w-m btn-primary" href="/games?new">Add New Game</a> -->
<?

use app\models\BookSeries;
use app\widgets\GridViewB4;
use app\widgets\LinkPagerB4;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\grid\ActionColumn;
use yii\helpers\Html;

if ($_GET['search']) {
    $query = BookSeries::find()->where(['like', 'title', $_GET['search']]);
} else {
    $query = BookSeries::find();
}


$provider = new ActiveDataProvider([
    'query' => $query,
]);

?>

<div>
    <div>
        <form method="get" action="/series">
            <div class="input-group mb-3" style="max-width: 300px;">
                <input placeholder="Search..." class="form-control" name="search" value="<?= $_GET['search']; ?>"/>
                <div class="input-group-append">
                    <button type="submit" class="btn btn-outline-primary">Search</button>
                </div>
            </div>
        </form>
    </div>
    <? if ($_GET['search']) { ?>
        <div class="alert alert-success" role="alert">
            Search results for: <strong><?= $_GET['search']; ?></strong>
        </div>
    <? } ?>
    <br/>
    <?php
    echo LinkPagerB4::widget([
        'pagination' => $provider->pagination,
    ]);
    ?>
</div>
<?
echo GridViewB4::widget([
    'dataProvider' => $provider,
    'layout' => "{summary}\n{items}",
    'columns' => [
        'id' => ['label' => 'Id', 'attribute' => 'id'],
        'title' => ['label' => 'Title', 'attribute' => 'title'],
        'promoted' => ['label' => 'Promoted', 'format' => 'raw', 'value' => function ($album) {
            return $album->promote == 1 ? '<span style="color: #1e7e34" class="fa fa-eye fa-fw"></span>' : '<span style="color: #a91c19" class="fa fa-eye fa-fw"></span>';
        }],
        'books' => ['label' => 'Books', 'attribute' => 'books_linked'],
        'buttons' => [
            'class' => ActionColumn::className(),
            'template' => ('{update} {delete}'),
            'buttons' => [
                'update' => function ($url, $model, $key) {
                    return Html::a('<span class="fa fa-edit fa-fw"></span>', '/series/' . $model->id, ['class' => 'btn  btn-primary btn-xs']);
                },
                'delete' => function ($url, $model, $key) {
                    return Html::a('<span class="fa fa-trash fa-fw"></span>', '/series/' . $model->id . "?delete", ['onclick' => 'return confirm("Really delete?");', 'class' => 'btn  btn-danger btn-xs']);
                },
            ],
        ]
    ],
]) ?>
