<a class="btn btn-w-m btn-primary" href="/books?new">Add New Book</a>
<?

use app\models\Books;
use app\models\BookSeries;

$selectedObj = $item->getBooks()->all();

$formInputs = [
    'title' => ['title' => 'Title'],
    'goodreads_id' => ['title' => 'Goodreads id', 'type' => 'integer'],
    'books_linked' => ['title' => 'Books Linked', 'type' => 'integer', 'value' => count($selectedObj)],
    'books_linked_old' => ['title' => 'Books Linked In DB', 'type' => 'disabled', 'value' => $item->books_linked],
    'promote' => ['title' => 'Promote', 'type' => 'boolean'],
    'visible' => ['title' => 'Visible', 'type' => 'boolean'],
    'tag' => ['title' => 'Tag', 'type' => 'boolean']
];

// -- Books

$sortBy = isset($_GET['sort']) ? $_GET['sort'] : "newest";

switch ($sortBy) {
    case "alphabet":
        $books = Books::find()->with('book2series.book')->orderBy(['title' => SORT_ASC])->all();
        break;
    case "newest":
    default:
        $books = Books::find()->with('book2series.book')->orderBy(['id' => SORT_DESC])->all();
}

$selected = [];

foreach ($selectedObj as $value) {
    $selected[$value->id] = true;
}

$dataFields = [];
$selecetField = [];

foreach ($books as $key => $book) {
    $bookItem = '
<span class="ms-selectedObject">
    <span>' . $book->title . '</span>
</span>';

    if ($selected[$book->id]) {
        $selecetField[$key] = $book;
    }
    $dataFields[$key] = $bookItem;
}

$formInputs['selectedBooks'] = [
    'title' => 'Books in Series', 'type' => 'list', 'data' => $selecetField, 'row' => function ($data) {
        return '<a href="/books/' . $data->id . '">' . $data->title . '</a>';
    }
];

$formInputs['books'] = [
    'sort' => [
        'alphabet' => "Alphabetical",
        'newest' => "Newest",
    ],
    'title' => 'Books',
    'type' => 'multiselect',
    'view' => $dataFields,
    'data' => $books,
    'itemContent' => function ($item) {
        return '<div>' . $item->title . '</div><div><img src="' . getenv('CDN_ROOT') . "/gallery/books/covers/" . $item->id . '.jpg" class="series-to-game-img" /></div>';
    },
    'value' => 'id',
    'selected' => $selected,
    'maxed' => true,
];

echo $this->render('@app/views/admin/blank/edit', array('item' => $item, 'post' => 'series', 'formInputs' => $formInputs));