<?

use app\models\Images;

$formInputs = [
    'title'=>['title'=>'Title'],
    'promote'=>['title'=>'Promote', 'type'=>'boolean'],
    'title_image_id'=>['title'=>'Image id', 'type'=>'integer'],
    'ordered'=>['title'=>'Order id', 'type'=>'integer'],
];

echo '<a class="btn btn-w-m btn-primary" href="/photos/session/' . $item->id . '">View/Edit photos</a>';
echo '<a class="btn btn-w-m btn-primary" href="/sessions/' . $item->id . '?multiupload">Multiupload</a>';

if (!empty($item->id)) {
    $formInputs['title_image_id'] = ['title' => 'Photo', 'type' => 'select', 'data' => Images::find()->where(['session_id' => $item->id])->all(), 'key' => 'id', 'value' => $item->title_image_id, 'label' => function($image, $props) {
        return 'ID: ' . $image['id'] . ' - ' . $image['title'] . '; ' . $image['persons'] . '; ' . $image['place'];
    }];
}

if (!empty($item->title_image_id)) {
    $formInputs['preview'] = [
        'title' => 'Title Photo',
        'type'=> 'showImage',
        'source' => 'gallery/500/' . $item->title_image_id . '.jpg'
    ];
}

echo $this->render('@app/views/admin/blank/edit', array('item'=>$item,'post'=>'sessions', 'formInputs'=>$formInputs));