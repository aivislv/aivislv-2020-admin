<div
        id="drop_zone">
    <p>Drag one or more files to this <i>drop zone</i>.</p>
</div>
<button id="doUpload">Upload!</button>
<ul id="file_list"></ul>
<script>
    const sessionId = <?=$session?>;
    const postUrl = '/ajax/sessionz';

    const dropBox = document.getElementById('drop_zone');
    const fileList = document.getElementById('file_list');
    const doUpload = document.getElementById('doUpload');

    const uploadFileList = [];

    defaultProps = {}

    _('li', {
        app: fileList, apc: [
            _('div', {
                apc: [
                    _('label', {apc: 'Default Place:'}),
                    defaultProps.place = _('input', {type: 'text', name: 'place', value: ""})
                ]
            }),
            _('div', {
                apc: [
                    _('label', {apc: 'Default Persons:'}),
                    defaultProps.persons = _('input', {type: 'text', name: 'persons', value: ""})
                ]
            }),
            _('div', {
                apc: [
                    _('label', {apc: 'Default Album id:'}),
                    defaultProps.album = _('input', {type: 'number', name: 'album', value: ""})
                ]
            }),
        ]
    });


    const appendFile = (file) => {
        const props = {}

        props.file = file;

        _('li', {
            app: fileList, apc: [
                _('div', {apc: file.name}),
                _('div', {
                    apc: [
                        _('label', {apc: 'Title:'}),
                        props.title = _('input', {type: 'text', name: 'title', value: ""})
                    ]
                }),
                _('div', {
                    apc: [
                        _('label', {apc: 'Place:'}),
                        props.place = _('input', {type: 'text', name: 'place', value: ""})
                    ]
                }),
                _('div', {
                    apc: [
                        _('label', {apc: 'Persons:'}),
                        props.persons = _('input', {type: 'text', name: 'persons', value: ""})
                    ]
                }),
                _('div', {
                    apc: [
                        _('label', {apc: 'Album id:'}),
                        props.album = _('input', {type: 'number', name: 'album', value: ""})
                    ]
                }),
                props.status = _('div', {
                    apc: 'Not uploaded yet'
                }),
            ]
        });

        return props;
    }

    const dropHandler = (ev) => {
        if (ev.dataTransfer.items) {
            // Use DataTransferItemList interface to access the file(s)
            [...ev.dataTransfer.items].forEach((item, i) => {
                // If dropped items aren't files, reject them
                if (item.kind === "file") {
                    uploadFileList.push(appendFile(item.getAsFile()))
                }
            });
        } else {
            // Use DataTransfer interface to access the file(s)
            [...ev.dataTransfer.files].forEach((file, i) => {
                uploadFileList.push(appendFile(file))
            });
        }
        ev.preventDefault();
    }

    const dragOverHandler = (ev) => {
        ev.preventDefault();
        ev.dataTransfer.dropEffect = "move";
    }

    const uploadFile = async (fileData) => {
        try {
            const formData = new FormData();

            const fetchOptions = {
                method: 'post',
                body: formData,
                credentials: "same-origin"
                // signal: abortController.signal,
            };

            const info = {
                title: fileData.title.value !== "" ? fileData.title.value : "",
                place: fileData.place.value !== "" ? fileData.place.value : defaultProps.place.value,
                persons: fileData.persons.value !== "" ? fileData.persons.value : defaultProps.persons.value,
                gid: fileData.album.value !== "" ? fileData.album.value : defaultProps.album.value,
                session_id: sessionId,
                song_id: ''
            }

            formData.append('image', fileData.file);
            formData.append('info[]', JSON.stringify(info));

            const result = await fetch(postUrl, fetchOptions)
            if (!result.ok) {
                throw new Error(result.statusText);
            }

            return true;
        } catch (e) {
            console.error(fileData, e);
            return false;
        }

    }

    const post = async () => {
        for (let a in uploadFileList) {
            if (!uploadFileList[a].uploaded) {
                uploadFileList[a].status.innerHTML = "In Progress";

                uploadFileList[a].uploaded = await uploadFile(uploadFileList[a]);
                if (uploadFileList[a].uploaded) {
                    uploadFileList[a].status.innerHTML = "Uploaded";
                } else {
                    uploadFileList[a].status.innerHTML = "Failed";
                }
            }
        }
    }

    dropBox.ondrop = dropHandler;
    dropBox.ondragover = dragOverHandler;
    doUpload.onclick = post;
</script>
<style>
    #drop_zone {
        border: 5px solid blue;
        width: 200px;
        height: 100px;
    }
</style>