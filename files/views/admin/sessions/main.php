<h1>Sessions</h1>
<a class="btn btn-w-m btn-primary" href="/sessions?new">Add New Session</a>
<br/><br/>
<?

use app\models\PhotoSession;
use app\widgets\GridViewB4;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\grid\ActionColumn;
use yii\helpers\Html;

$query = PhotoSession::find()->orderBy(['id' => SORT_DESC]);

$provider = new ActiveDataProvider([
    'query' => $query,
]);

echo  GridViewB4::widget([
    'dataProvider' => $provider,
    'columns' => [
        'id'=>['label'=>'Id','attribute'=>'id'],
        'title'=>['label'=>'Title','attribute'=>'title'],
        'ordered'=>['label'=>'Order','attribute'=>'ordered'],
        'promote'=>['label'=>'Promote', 'format'=>'raw', 'value'=>function($session) {
            return $session->promote == 1 ? '<span style="color: #1e7e34" class="fa fa-eye fa-fw"></span>' : '<span style="color: #a91c19" class="fa fa-eye fa-fw"></span>';
        }],
        'buttons'=>[
            'class' => ActionColumn::className(),
            'template' => ('{update} {upload} {delete}'),
            'buttons'=>[
                'update' => function ($url, $model, $key) {return Html::a('<span class="fa fa-edit fa-fw"></span>', '/sessions/' . $model->id, ['class'=>'btn  btn-primary btn-xs']);},
                'upload' => function ($url, $model, $key) {
                    return Html::a('<span class="fa fa-upload fa-fw"></span>', '/sessions/' . $model->id . "?multiupload", ['class' => 'btn  btn-primary btn-xs']);
                },
                'delete' => function ($url, $model, $key) {return Html::a('<span class="fa fa-trash fa-fw"></span>', '/sessions/' . $model->id . "?delete", ['onclick'=>'return confirm("Really delete?");','class'=>'btn  btn-danger btn-xs']);},
            ],
        ]
    ],
]) ?>
