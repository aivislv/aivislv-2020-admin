<?
if (!isset($primary)) {
    $primary = 'id';
}

if ($item) {
  $errors = $item->getErrors();
} else {
  $errors = [];
}

if (!function_exists('genereateLabel')) {
    function genereateLabel($label, $item, $props)
    {
        if ($label instanceof Closure) {
            $response = call_user_func($label, $item, $props);
        } else {
            $response = $item->{$label};
        }

        return $response;
    }
}

?>
<form class="form-for-<?= $post; ?>" style="padding-top:20px; margin:auto; position: relative; padding-bottom: 50px"
      id="form-<?= rand(1, 9999); ?>" enctype="multipart/form-data" method="post" class="form-horizontal"
      action="<?= !isset($postFix) ? ('/' . $post . ($item->isNewRecord ? '?new' : ('/' . $item->{$primary}))) : $postFix; ?>">
    <input type="hidden" name="save" value="1"/>

    <? foreach ($formInputs as $key => $input) {
        if (isset($input['value']) || isset($input['text'])) {
            $displayValue = $input['value'];
        } else {
            if ($item->hasAttribute($key)) {
                $displayValue = $item->{$key};
            }
        }

        if (!isset($input['type']) || $input['type']!='hidden') { ?>
            <div class="form-group form-group-<?= $key; ?> <?= isset($item->errors[$key]) ? "has-error" : "" ?>">
                <div class="alert alert-danger no-error col-sm-10" style="float: right"
                     role="alert"><?= isset($errors[$key]) ? $errors[$key][0] : '' ?></div>
                <div class="row">
                    <label class="col-sm-2 col-form-label a-form-label"><?= $input['title']; ?></label>
                    <ul class="col-sm-10" style="text-align: left">
                        <?
                        if (!isset($input['type']) || $input['type'] == 'input') { ?>
                        <input <?= isset($input['required']) ? 'required' : ''; ?> <?= isset($input['placeholder']) ? 'placeholder="' . $input['placeholder'] . '"' : ''; ?>
                                type="text" class="form-control" value="<?= $displayValue; ?>" name="<?= $key; ?>">
                        <? } else {

                        // ---------------- PASSWORD

                    if ($input['type'] == 'password') { ?>
                        <input <?=isset($input['required']) ? 'required' : '';?> type="password" class="form-control" name="<?= $key; ?>"/>
                    <? }

                    if ($input['type'] == 'skip') {}

                // ---------------- EMAIL

                    if ($input['type'] == 'email') {
                      ?>
                        <input type="email" <?=isset($input['required']) ? 'required' : '';?> class="form-control" name="<?= $key; ?>" value="<?= $displayValue;?>"/>
                    <? }

                    // ---------------- BOOLEAN

                    if ($input['type']=='boolean') { ?>
                        <input type="checkbox" class="i-checks" <?=$displayValue==1?'checked':'';?> name="<?= $key; ?>"/>
                    <? }

                    // ---------------- TEXT (text-area)

                    if ($input['type'] == 'text') { ?>
                        <textarea <?=isset($input['placeholder'])?'placeholder="'.$input['placeholder'].'"':'';?> class="form-control" id="<?= $key; ?>" name="<?= $key; ?>"><?= $displayValue; ?></textarea>
                        <script>
                            var MyEditor = ClassicEditor.create( document.querySelector( '#<?= $key; ?>' ), {
                                toolbar: ['heading', '|', 'bold', 'italic', 'link', 'bulletedList', 'numberedList', 'blockQuote'],
                                heading: {
                                  options: [
                                    { model: 'paragraph', title: 'Paragraph', class: 'ck-heading_paragraph' },
                                    { model: 'heading1', view: 'h1', title: 'Heading 1', class: 'ck-heading_heading1' },
                                    { model: 'heading2', view: 'h2', title: 'Heading 2', class: 'ck-heading_heading2' },
                                    { model: 'heading3', view: 'h3', title: 'Heading 3', class: 'ck-heading_heading3' },
                                    { model: 'heading4', view: 'h4', title: 'Heading 4', class: 'ck-heading_heading4' },
                                  ]
                                },
                              }
                            )
                            .catch( error => {
                                console.log( error );
                            } );
                        </script>
                    <? }

                    // ---------------- TEMPLATE (text-area for email templates)

                    if ($input['type'] == 'template') { ?>
                      <textarea <?=isset($input['placeholder'])?'placeholder="'.$input['placeholder'].'"':'';?> class="form-control" id="<?= $key; ?>" name="<?= $key; ?>"  ><?= $displayValue; ?></textarea>
                      <script>
                        var MyEditor = ClassicEditor.create( document.querySelector( '#<?= $key; ?>' ), {
                          toolbar: [ 'heading', '|', 'bold', 'italic', 'link', 'bulletedList', 'numberedList'],
                          heading: {
                            options: [
                              { model: 'paragraph', title: 'Paragraph', class: 'ck-heading_paragraph' },
                              { model: 'heading1', view: 'h1', title: 'Heading 1', class: 'ck-heading_heading1' },
                              { model: 'heading2', view: 'h2', title: 'Heading 2', class: 'ck-heading_heading2' },
                              { model: 'heading3', view: 'h3', title: 'Heading 3', class: 'ck-heading_heading3' },
                              { model: 'heading4', view: 'h4', title: 'Heading 4', class: 'ck-heading_heading4' },
                            ]
                          }
                          },
                        )
                          .catch( error => {
                            console.log( error );
                          } );
                      </script>
                    <? }

                    // ---------------- TEMPLATE_MULTIPART (text-area for email templates)

                    if ($input['type'] == 'template_multipart') {

                      $multipartData = explode('#section.start#', $displayValue);

                      $parts = [];

                      if (count($multipartData)>1) {
                        foreach ($multipartData as $part) {

                          $pieces = explode("#section.end#", $part);

                          if (count($pieces) > 1) {
                            $parts[] = ['text' => $pieces[0], 'edit'=>false];
                            $parts[] = ['text' => $pieces[1], 'edit'=>true];
                          } else {
                            $parts[] = ['text' => $pieces[0], 'edit'=>true];
                          }
                        }

                      } else {
                        $parts[] = ['text' => $multipartData[0], 'edit'=>true];
                      }

                       foreach ($parts as $pieceKey => $piece) {

                      if ($piece['edit']) {?>
                  <textarea <?=isset($input['placeholder'])?'placeholder="'.$input['placeholder'].'"':'';?> class="form-control" id="multipart_template_<?=$pieceKey;?>" name="<?= $key . "[" . $pieceKey . "]"; ?>"><?= $piece['text']; ?></textarea>
                  <script>
                    var MyEditor = ClassicEditor.create( document.querySelector( '#multipart_template_<?=$pieceKey;?>' ), {
                        toolbar: [ 'heading', '|', 'bold', 'italic', 'link', 'bulletedList', 'numberedList'],
                        heading: {
                          options: [
                            { model: 'paragraph', title: 'Paragraph', class: 'ck-heading_paragraph' },
                            { model: 'heading1', view: 'h1', title: 'Heading 1', class: 'ck-heading_heading1' },
                            { model: 'heading2', view: 'h2', title: 'Heading 2', class: 'ck-heading_heading2' },
                            { model: 'heading3', view: 'h3', title: 'Heading 3', class: 'ck-heading_heading3' },
                            { model: 'heading4', view: 'h4', title: 'Heading 4', class: 'ck-heading_heading4' },
                          ]
                        }
                      },
                    )
                      .catch( error => {
                        console.log( error );
                      } );
                  </script>
                    <? } else {
                        echo $piece['text'];
                      }
                    }?>

                <? }

                    // ---------------- INTEGER

                    if ($input['type'] == 'integer') {
                        $id = 'MyInt' . rand(1000, 9999) . rand(1000, 9999);
                    ?>
                        <input <?=isset($input['required']) ? 'required' : '';?> <?=isset($input['placeholder'])?'placeholder="'.$input['placeholder'].'"':'';?> step="1" type="number" id="<?=$id;?>" class="form-control" value="<?= $displayValue; ?>" name="<?= $key; ?>"/>
                        <script>
                            $('#<?=$id;?>').bind('change', function (event) {
                                event.currentTarget.value = parseFloat(this.value).toFixed(0);
                                return true;
                            })
                        </script>
                    <? }

                    // ---------------- FLOAT

                    if ($input['type'] == 'float') {
                        $id = 'MyFloat' . rand(1000, 9999) . rand(1000, 9999);
                        ?>
                        <input <?=isset($input['required']) ? 'required' : '';?> <?=isset($input['placeholder'])?'placeholder="'.$input['placeholder'].'"':'';?> min="0" max="100000" id="<?=$id;?>" step="0.01" data-decimal="<?=isset($input['decimal'])?2:$input['decimal'];?>" type="number" class="form-control" value="<?= $displayValue; ?>" name="<?= $key; ?>"/>
                        <script>
                            $('#<?=$id;?>').bind('change', function (event) {
                                event.currentTarget.value = parseFloat(this.value).toFixed(event.currentTarget.dataset.decimal);
                                return true;
                            })
                        </script>
                    <? }

                    // ---------------- DISABLED FIELD

                    if ($input['type'] == 'disabled') { ?>
                        <input type="text" disabled  class="form-control" value="<?= $displayValue; ?>" />
                    <? }

                    // ---------------- UPLOAD FIELD

                    if ($input['type'] == 'upload') { ?>
                        <input type="file" class="form-control" <?=isset($input['accept'])?'accept="' . $input['accept'] . '"':'';?> name="<?= $key; ?>"/>
                    <? }

                    // ---------------- ECHO (output plain text)

                    if ($input['type'] == 'echo') {
                        echo $input['text'];
                    }

                    // ---------------- SHOWIMAGE (display image)

                    if ($input['type'] == 'showImage') {
                        if ($input['source']) {
                            ?>
                            <img src="<?=getenv('CDN_ROOT') . "/" . $input['source'];?>" style="max-width: 300px; max-height: 300px;"/>
                    <?}}

                    // ---------------- MULTISELECT

                    if ($input['type'] == 'multiselect') {
                    $id = 'MyMultiSelect' . rand(1000, 9999) . rand(1000, 9999);
                    ?>
                            <div class="<?= isset($input['maxed']) ? "maxOut" : ""; ?>">
                                <? if (isset($input['sort']) && !$item->isNewRecord) {
                                    $link = (!isset($postFix) ? ('/' . $post . ('/' . $item->{$primary})) : $postFix) . "?sort=";
                                    echo '<div>';
                                    foreach ($input['sort'] as $sortedKey => $sorted) {
                                        echo '<a class="btn btn-w-m btn-primary" href="' . $link . $sortedKey . '">' . $sorted . '</a>';
                                    }
                                    echo '</div>';
                                } ?>
                                <div></div>
                                <ul id="<?= $id; ?>" class="multiselect">
                                    <? foreach ($input['data'] as $itemKey => $data) { ?>
                                        <li data-selected=<?= isset($input['selected'][$data->{$input['value']}]) ? 'true' : 'false'; ?> value="<?= $data->{$input['value']}; ?>">
                                            <? if (isset($input['itemContent'])) {
                                                echo $input['itemContent']($data);
                                            } else {
                                                echo isset($input['label']) ? $data->{$input['label']} : $input['view'][$itemKey];
                                            } ?>
                                        </li>
                                    <? } ?>
                                </ul>
                        <select id="<?= $id . 'SEL'; ?>" multiple="multiple" style="display: none"
                                name="<?= $key; ?>[]">
                            <? foreach ($input['data'] as $data) { ?>
                                <option <?= isset($input['selected'][$data->{$input['value']}]) ? 'selected' : ''; ?>
                                        value="<?= $data->{$input['value']}; ?>">Opt
                                </option>
                            <? } ?>
                        </select>
                        <script>
                            $('#<?=$id;?>').multiList();
                            $('#<?=$id;?>').bind('multiList.elementChecked', function (val, id) {
                                var option = $('#<?=$id . 'SEL';?> [value=' + id + ']')[0];
                                if (option) {
                                    option.setAttribute('selected','selected');
                                }
                            });
                            $('#<?=$id;?>').bind('multiList.elementUnchecked', function (val, id) {
                                var option = $('#<?=$id . 'SEL';?> [value=' + id + ']')[0];
                                if (option) {
                                    option.removeAttribute('selected');
                                }
                            })
                        </script>
                    </div>
                    <? }

                // ---------------- LIST

                if ($input['type'] == 'list') {

                ?>
                            <ul class="list-group" style="max-height: 1000px; overflow: auto">
                                <? foreach ($input['data'] as $data) {
                        echo '<li class="list-group-item">' . call_user_func($input['row'], $data) . '</li>';
                    } ?>
                </ul>
                <? }

                // ---------------- SELECT

                if ($input['type'] == 'select') {

                    ?>
                    <select class="form-control" name="<?= $key; ?>">
                        <option label=" "></option>
                        <? foreach ($input['data'] as $data) { ?>
                            <option <?= $displayValue == $data->{$input['key']} ? 'selected' : ''; ?>
                                    value="<?= $data->{$input['key']}; ?>"><?= genereateLabel($input['label'], $data, $input); ?></option>
                            <? } ?>
                        </select>
                    <? }

                    // ---------------- SELECTSIMPLE

                    if ($input['type'] == 'selectSimple') { ?>
                    <select class="form-control" name="<?= $key; ?>" <?= $input['disabled'] ? "disabled" : ""; ?>
                            size="<?= $input['lines'] ? $input['lines'] : 1; ?>">
                        <? foreach ($input['data'] as $dateKey => $dataValue) { ?>
                            <option <?= $input['disableSelect'] ? "disabled" : ""; ?> <?= $displayValue == $dateKey ? 'selected' : ''; ?>
                                    value="<?= $dateKey; ?>"><?= $dataValue; ?></option>
                        <? } ?>
                    </select>
                <? }

                    // ---------------- DATE

                    if ($input['type'] == 'date') { ?>
                        <input type="text" class="form-control datepicker" value="<?= $displayValue; ?>" name="<?= $key; ?>"/>
                    <? }
                } ?>
            </div>
        </div>
    <? } else { ?>
            <input type="hidden" value="<?= $displayValue; ?>" name="<?= $key; ?>"/>
       <? }
    } ?>

    <input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>" />

    <? if (!isset($saveButton) || $saveButton) { ?>
    <div class="form-group <?=isset($item->errors[$key])?"has-error":"" ?>"><label class="col-sm-2 control-label"></label>
        <div class="alert alert-danger no-error" role="alert"><?=isset($errors[$key])?$errors[$key][0]:''?></div><div style="text-align: right;" class="col-sm-12">
            <button class="btn btn-w-m btn-primary" type="submit"><?=$saveTitle ? $saveTitle : 'Save';?></button>
        </div>
    </div>
    <? } ?>
</form>
