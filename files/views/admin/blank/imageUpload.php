<?

Yii::$app->view->registerCssFile('/vendors/theme/css/plugins/dropzone/basic.css');
Yii::$app->view->registerCssFile('/vendors/theme/css/plugins/dropzone/dropzone.css');

Yii::$app->view->registerJsFile('/vendors/theme/js/plugins/dropzone/dropzone.js');

?>

<form id="my-awesome-dropzone" class="dropzone dz-clickable" action="/admin/albums/<?=$album->id;?>?upload">
    <div class="dropzone-previews"></div>
    <button type="submit" class="btn btn-primary pull-right">Augšuplādēt</button>
    <div class="dz-default dz-message"><span>Ievelciet failus šeit</span></div></form>

<div class="lightBoxGallery" style="margin-top: 20px">

    <? foreach ($data['data'] as $image){ ?>
    <div class="thumbnail">
        <a  href="/gallery/<?=$image->album_id;?>/<?=$image->id;?>.jpg" title="<?=$image->title;?>" data-gallery="">
        <img src="/gallery/<?=$image->album_id;?>/<?=$image->id;?>.jpg">
        </a>
        <span class="title"><?=$image->title;?></span>
        <a title="Labot" href="/admin/images/<?=$image->id;?>" class="glyphicon glyphicon-pencil edit"></a>
        <? if (!$image->checkIfMain()) {?>
            <a title="Galvenais attēls" href="/admin/images/<?=$image->id;?>?main" class="glyphicon glyphicon-flag flag"></a>
        <? } ?>
        <a title="Dzēst" href="/admin/images/<?=$image->id;?>?delete" class="glyphicon glyphicon-trash remove"></a>
    </div>
    <? } ?>

</div>

<script>
    $(document).ready(function(){

        Dropzone.options.myAwesomeDropzone = {

            autoProcessQueue: false,
            uploadMultiple: true,
            parallelUploads: 100,
            maxFiles: 100,

            // Dropzone settings
            init: function() {
                var myDropzone = this;

                this.element.querySelector("button[type=submit]").addEventListener("click", function(e) {
                    e.preventDefault();
                    e.stopPropagation();
                    myDropzone.processQueue();
                });
                this.on("sendingmultiple", function() {
                });
                this.on("successmultiple", function(files, response) {
                    document.location.href = '/admin/albums/<?=$album->id;?>';
                });
                this.on("errormultiple", function(files, response) {
                });
            }

        }

    });
</script>
