<a class="btn btn-w-m btn-primary" href="/gametags?new">Add New Game Tag</a>
<?

use app\models\Games;
use app\models\GameTagCategories;
use app\models\GameTags;

$selectedObj = $item->getGames()->all();

$formInputs = [
    'tag' => ['title' => 'Tag'],
    'promote' => ['title' => 'Promote', 'type' => 'boolean'],
];

// -- Games

$sortBy = isset($_GET['sort']) ? $_GET['sort'] : "newest";

switch ($sortBy) {
    case "alphabet":
        $games = Games::find()->with('games2tags.game')->where(['expansion' => 0])->orderBy(['title' => SORT_ASC])->all();
        break;
    case "newest":
    default:
        $games = Games::find()->with('games2tags.game')->where(['expansion' => 0])->orderBy(['id' => SORT_DESC])->all();
}


$selected = [];

foreach ($selectedObj as $value) {
    $selected[$value->id] = true;
}

$dataFields = [];
$selecetField = [];

foreach ($games as $key => $game) {
    $gameItem = '
    <span class="ms-selectedObject">
        <span>' . $game->title . '</span>
    </span>';

    if ($selected[$game->id]) {
        $selecetField[$key] = $game;
    }
    $dataFields[$key] = $gameItem;
}

if (!empty($item->id)) {
    $formInputs['category_id'] = ['title' => 'Category', 'type' => 'select', 'data' => GameTagCategories::find()->all(), 'key' => 'id', 'value' => $item->category_id, 'label' => function ($cat, $props) {
        return $cat['name'];
    }];
}


$formInputs['selectedGames'] = [
    'title' => 'Games in Tag', 'type' => 'list', 'data' => $selecetField, 'row' => function ($data) {
        return '<a href="/games/' . $data->id . '">' . $data->title . '</a>';
    }
];

$formInputs['games'] = [
    'sort' => [
        'alphabet' => "Alphabetical",
        'newest' => "Newest",
    ],
    'title' => 'Games',
    'type' => 'multiselect',
    'view' => $dataFields,
    'data' => $games,
    'itemContent' => function ($item) {
        return '<div>' . $item->title . '</div><div><img src="' . getenv('CDN_ROOT') . "/gallery/games/" . $item->id . '.jpg" class="series-to-game-img" /></div>';
    },
    'value' => 'id',
    'selected' => $selected,
    'maxed' => true,
];

echo $this->render('@app/views/admin/blank/edit', array('item' => $item, 'post' => 'gametags', 'formInputs' => $formInputs));