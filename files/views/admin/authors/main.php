<h1>Book Authors</h1>
<a class="btn btn-w-m btn-primary" href="/authors?new">Add New Author</a>
<br/><br/>
<!-- <a class="btn btn-w-m btn-primary" href="/games?new">Add New Game</a> -->
<?

use app\models\BookAuthors;
use app\widgets\GridViewB4;
use app\widgets\LinkPagerB4;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\grid\ActionColumn;
use yii\helpers\Html;

if ($_GET['search']) {
    $query = BookAuthors::find()->where(['like', 'name_eng', $_GET['search']]);
} else {
    $query = BookAuthors::find();
}


$provider = new ActiveDataProvider([
    'query' => $query,
]);

?>

<div>
    <div>
        <form method="get" action="/series">
            <div class="input-group mb-3" style="max-width: 300px;">
                <input placeholder="Search..." class="form-control" name="search" value="<?= $_GET['search']; ?>"/>
                <div class="input-group-append">
                    <button type="submit" class="btn btn-outline-primary">Search</button>
                </div>
            </div>
        </form>
    </div>
    <? if ($_GET['search']) { ?>
        <div class="alert alert-success" role="alert">
            Search results for: <strong><?= $_GET['search']; ?></strong>
        </div>
    <? } ?>
    <br/>
    <?php
    echo LinkPagerB4::widget([
        'pagination' => $provider->pagination,
    ]);
    ?>
</div>
<?
echo GridViewB4::widget([
    'dataProvider' => $provider,
    'layout' => "{summary}\n{items}",
    'columns' => [
        'id' => ['label' => 'Id', 'attribute' => 'id'],
        'name_eng' => ['label' => 'Name', 'attribute' => 'name_eng'],
        'books' => ['label' => 'Books', 'attribute' => 'books_linked'],
        'buttons' => [
            'class' => ActionColumn::className(),
            'template' => ('{update} {delete}'),
            'buttons' => [
                'update' => function ($url, $model, $key) {
                    return Html::a('<span class="fa fa-edit fa-fw"></span>', '/authors/' . $model->id, ['class' => 'btn  btn-primary btn-xs']);
                },
                'delete' => function ($url, $model, $key) {
                    return Html::a('<span class="fa fa-trash fa-fw"></span>', '/authors/' . $model->id . "?delete", ['onclick' => 'return confirm("Really delete?");', 'class' => 'btn  btn-danger btn-xs']);
                },
            ],
        ]
    ],
]) ?>
