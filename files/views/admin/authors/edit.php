<a class="btn btn-w-m btn-primary" href="/authors?new">Add New Author</a>
<?

use app\models\Books;
use app\models\BookSeries;

$selectedObj = $item->getBooks()->all();

$formInputs = [
    'name_eng' => ['title' => 'Name english'],
    'name_orig' => ['title' => 'Name original'],
    'goodreads_id' => ['title' => 'Goodreads id', 'type' => 'integer'],
    'books_linked' => ['title' => 'Books Linked', 'type' => 'integer', 'value' => count($selectedObj)],
    'books_linked_old' => ['title' => 'Books Linked In DB', 'type' => 'disabled', 'value' => $item->books_linked],
    'promote' => ['title' => 'Promote', 'type' => 'boolean'],
    'visible' => ['title' => 'Visible', 'type' => 'boolean'],
];

// -- Authors

$books = Books::find()->with('book2authors.book')->orderBy(['title' => SORT_ASC])->all();

$selected = [];

foreach ($selectedObj as $value) {
    $selected[$value->id] = true;
}

$dataFields = [];
$selecetField = [];

foreach ($books as $key => $book) {
    $bookItem = '
<span class="ms-selectedObject">
    <span>' . $book->title . '</span>
</span>';

    if ($selected[$book->id]) {
        $selecetField[$key] = $book;
    }
    $dataFields[$key] = $bookItem;
}

$formInputs['selectedBooks'] = [
    'title' => 'Authors Books', 'type' => 'list', 'data' => $selecetField, 'row' => function ($data) {
        return '<a href="/books/' . $data->id . '">' . $data->title . '</a>';
    }
];

$formInputs['books'] = [
    'title' => 'Books',
    'type' => 'multiselect',
    'view' => $dataFields,
    'data' => $books,
    'value' => 'id',
    'selected' => $selected
];

echo $this->render('@app/views/admin/blank/edit', array('item' => $item, 'post' => 'authors', 'formInputs' => $formInputs));