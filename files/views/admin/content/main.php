<h1>Content</h1>
<a class="btn btn-w-m btn-primary" href="/content?new">Add New Content item</a>
<br/><br/>
<?
use \yii\data\ActiveDataProvider;
use \yii\grid\GridView;
use \yii\grid\ActionColumn;
use \yii\helpers\Html;

$query = \app\models\About::find();

$provider = new ActiveDataProvider([
    'query' => $query,
]);

echo  \app\widgets\GridViewB4::widget([
    'dataProvider' => $provider,
    'columns' => [
        'id'=>['label'=>'Id','attribute'=>'id'],
        'title'=>['label'=>'Title','attribute'=>'title'],
        'buttons'=>[
            'class' => ActionColumn::className(),
            'template'=>('{update} {delete}'),
            'buttons'=>[
                'update' => function ($url, $model, $key) {return Html::a('<span class="fa fa-edit fa-fw"></span>', '/content/' . $model->id, ['class'=>'btn  btn-primary btn-xs']);}
            ],
        ]
    ],
]) ?>
