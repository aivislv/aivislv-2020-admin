<?

$formInputs = [
    'photo'=>['title'=>'Anime image', 'source' => 'gallery/anime/' . $item->id . '.jpg', 'type'=> 'showImage'],
    'title'=>['title'=>'Title'],
    'wish_weight' => ['title' => 'Wish Weight', 'type' => 'integer'],
    'mal_id'=>['title'=>'MAL id', 'type'=>'disabled'],
    'rating'=>['title'=>'Rating', 'type'=>'disabled'],
    'score'=>['title'=>'Score', 'type'=>'disabled'],
    'rank'=>['title'=>'Bgg Rank', 'type'=>'disabled'],
    'status' => ['title'=>'Type', 'type'=>'selectSimple','data'=>[
        '1' => 'Currently watching',
        '2' => 'Watched',
        '3' => 'On Hold',
        '4' => 'Dropped',
        '5' => '???? (Check status?)',
        '6' => 'Wishlist',
        ]],
    'synopsis'=>['title'=>'Description', 'type'=>'echo', 'text'=> $item->synopsis],
];

echo $this->render('@app/views/admin/blank/edit', array('item'=>$item,'post'=>'animes', 'formInputs'=>$formInputs));