<h1>Game prices</h1>
<!-- <a class="btn btn-w-m btn-primary" href="/games?new">Add New Game</a> -->
<?

use app\models\Games;
use app\widgets\GridViewB4;
use app\widgets\LinkPagerB4;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\grid\ActionColumn;
use yii\helpers\Html;

if ($_GET['search']) {
    $query = Games::find()->where(['like', 'games.title', $_GET['search']])->andWhere(['games.type' => 2])->orderBy(['title' => SORT_ASC]);
} else {
    $query = Games::find()->where(['type' => 2])->orderBy(['games.title' => SORT_ASC]);
}

$provider = new ActiveDataProvider([
    'query' => $query,
]);

?>

<div>
    <div>
        <form method="get" action="/gameprices">
            <div class="input-group mb-3" style="max-width: 300px;">
                <input placeholder="Search..." class="form-control" name="search" value="<?= $_GET['search']; ?>"/>
                <div class="input-group-append">
                    <button type="submit" class="btn btn-outline-primary">Search</button>
                </div>
            </div>
        </form>
    </div>
    <? if ($_GET['search']) { ?>
        <div class="alert alert-success" role="alert">
            Search results for: <strong><?= $_GET['search']; ?></strong>
        </div>
    <? } ?>
    <br/>
    <?php
    echo LinkPagerB4::widget([
        'pagination' => $provider->pagination,
    ]);
    ?>
</div>
<?
echo GridViewB4::widget([
    'dataProvider' => $provider,
    'layout' => "{summary}\n{items}",
    'columns' => [
        'id' => ['label' => 'Id', 'attribute' => 'id'],
        'title' => ['label' => 'Title', 'attribute' => 'title'],
        'preview' => ['label' => '', 'format' => 'raw', 'value' => function ($game) {
            return '<img src="https://cdn.aivis.lv/gallery/games/' . $game->id . '.jpg" />';
        }],
        'price' => ['label' => 'Lowest price', 'format' => 'raw', 'value' => function ($game) {
            return $game->getGamePrices()->where(['in', 'status', ['new', 'remember']])->orderBy(['status' => SORT_DESC, 'price' => SORT_ASC])->one()->price;
        }],
        'priceCount' => ['label' => 'Price count', 'format' => 'raw', 'value' => function ($game) {
            return $game->getGamePrices()->where(['in', 'status', ['new', 'remember']])->count();
        }],
        'buttons' => [
            'class' => ActionColumn::className(),
            'template' => ('{list}'),
            'buttons' => [
                'list' => function ($url, $model, $key) {
                    return Html::a('<span class="fa fa-edit fa-fw"></span>', '/gameprices/?list=' . $model->id, ['class' => 'btn  btn-primary btn-xs']);
                },
            ],
        ]
    ],
]) ?>
