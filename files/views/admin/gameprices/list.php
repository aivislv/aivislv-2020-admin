<h1>Game prices, list prices</h1>
<!-- <a class="btn btn-w-m btn-primary" href="/games?new">Add New Game</a> -->
<?

use app\models\GamePrices;
use app\models\Games;
use app\widgets\GridViewB4;
use app\widgets\LinkPagerB4;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\grid\ActionColumn;
use yii\helpers\Html;

if ($_GET['list']) {
    $game = Games::findOne($_GET['list']);
}

?>
<h3><?= $game->title; ?></h3>
<?php

$query = GamePrices::find()->where(['item_id' => $game->id])->andWhere(['in', 'status', ['new', 'remember']])->orderBy(['date' => SORT_DESC]);

$provider = new ActiveDataProvider([
    'query' => $query,
]);


echo GridViewB4::widget([
    'dataProvider' => $provider,
    'layout' => "{summary}\n{items}",
    'columns' => [
        'id' => ['label' => 'Id', 'attribute' => 'id'],
        'link' => ['label' => 'Link', 'format' => 'raw', 'value' => function ($item) {
            return '<a href="' . $item->link . '">Open</a>';
        }],
        'status' => ['label' => 'Status', 'attribute' => 'status'],
        'price' => ['label' => 'Price', 'attribute' => 'price'],
        'shipping' => ['label' => 'Shipping', 'attribute' => 'shipping'],
        'currency' => ['label' => 'Cur', 'attribute' => 'currency'],
        'date' => ['label' => 'Date', 'attribute' => 'date'],
        'source' => ['label' => '???', 'attribute' => 'source'],
        'country' => ['label' => 'Where', 'attribute' => 'country'],
        'details' => ['label' => 'Details', 'format' => 'raw', 'attribute' => 'details'],
        'buttons' => [
            'class' => ActionColumn::className(),
            'template' => ('{remember} {ignore}'),
            'buttons' => [
                'ignore' => function ($url, $model, $key) {
                    return Html::a('<span class="fa fa-ban fa-fw"></span>', '/gameprices/?list=' . $model->item_id . '&status=ignore&id=' . $model->id, ['class' => 'btn  btn-primary btn-xs']);
                },
                'remember' => function ($url, $model, $key) {
                    return Html::a('<span class="fa fa-check fa-fw"></span>', '/gameprices/?list=' . $model->item_id . '&status=remember&id=' . $model->id, ['class' => 'btn  btn-primary btn-xs']);
                },
            ],
        ]
    ],
]);
