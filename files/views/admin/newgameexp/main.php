<h1>Game Expansion Importer</h1>

<?

use app\models\Games;
use app\models\GameExpansionImporter;
use app\widgets\GridViewB4;
use app\widgets\LinkPagerB4;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\grid\ActionColumn;
use yii\helpers\Html;

if (isset($_GET['filter'])) {
    $filterBy = $_GET['filter'];
} else {
    $filterBy = "new";
}

if ($_GET['search']) {
    $query = GameExpansionImporter::find()->where(['like', 'title', $_GET['search']]);

    if ($filterBy !== "all") {
        $query = $query->andWhere(['status' => $filterBy]);
    }
} else {
    if ($filterBy !== "all") {
        $query = GameExpansionImporter::find()->where(['status' => $filterBy]);
    } else {
        $query = GameExpansionImporter::find();
    }
}

$provider = new ActiveDataProvider([
    'query' => $query->orderBy(['parent_id' => SORT_ASC, 'title' => SORT_ASC]),
]);

?>

<div>
    <div>
        <form method="get" action="/newgameexp">
            <div class="input-group mb-3" style="max-width: 300px;">
                <input hidden value="<?= $filterBy; ?>" name="filter"/>
                <input placeholder="Search..." class="form-control" name="search" value="<?= $_GET['search']; ?>"/>
                <div class="input-group-append">
                    <button type="submit" class="btn btn-outline-primary">Search</button>
                    <button type="button" class="btn btn-outline-primary"
                            onclick="document.location.href = '/newgameexp?filter=new'">
                        <span class="fa fa-plus fa-fw" tooltip="New"></span>
                    </button>
                    <button type="button" class="btn btn-outline-primary"
                            onclick="document.location.href = '/newgameexp?filter=ignore'">
                        <span class="fa fa-ban fa-fw" tooltip="Ignore"></span>
                    </button>
                    <button type="button" class="btn btn-outline-primary"
                            onclick="document.location.href = '/newgameexp?filter=postpone'">
                        <span class="fa fa-clock fa-fw" tooltip="Postpone"></span>
                    </button>
                    <button type="button" class="btn btn-outline-primary"
                            onclick="document.location.href = '/newgameexp?filter=review'">
                        <span class="fa fa-question fa-fw" tooltip="Review"></span>
                    </button>
                    <button type="button" class="btn btn-outline-primary"
                            onclick="document.location.href = '/newgameexp?filter=precessed'">
                        <span class="fa fa-check fa-fw" tooltip=""></span>
                    </button>
                    <button type="button" class="btn btn-outline-primary"
                            onclick="document.location.href = '/newgameexp?filter=pnp'">
                        <span class="fa fa-print fa-fw" tooltip="Print'n'play"></span>
                    </button>
                    <button type="button" class="btn btn-outline-primary"
                            onclick="document.location.href = '/newgameexp?filter=fan'">
                        <span class="fa fa-user fa-fw" tooltip="Fan-made"></span>
                    </button>
                    <button type="button" class="btn btn-outline-primary"
                            onclick="document.location.href = '/newgameexp?filter=insert'">
                        <span class="fa fa-box fa-fw" tooltip="Insert"></span>
                    </button>
                    <button type="button" class="btn btn-outline-primary"
                            onclick="document.location.href = '/newgameexp?filter=buy-first'">
                        <span class="fa fa-dollar-sign fa-fw" tooltip="Buy First"></span>
                    </button>
                    <button type="button" class="btn btn-outline-primary"
                            onclick="document.location.href = '/newgameexp?filter=all'">All
                    </button>
                </div>
            </div>
        </form>
    </div>
    <? if ($_GET['search']) { ?>
        <div class="alert alert-success" role="alert">
            Search results for: <strong><?= $_GET['search']; ?></strong>
        </div>
    <? } ?>
    <br/>
    <?php
    echo LinkPagerB4::widget([
        'pagination' => $provider->pagination,
    ]);
    ?>
</div>
<?
echo GridViewB4::widget([
    'dataProvider' => $provider,
    'layout' => "{summary}\n{items}",
    'columns' => [
        'id' => ['label' => 'Id', 'attribute' => 'id'],
        'title' => ['label' => 'Title', 'attribute' => 'title'],
        'type' => ['label' => 'Type', 'attribute' => 'type'],
        'status' => ['label' => 'status', 'attribute' => 'status'],
        'buttons' => [
            'class' => ActionColumn::className(),
            'template' => ('{ignore} {postpone} {review} {processed} {print} {fan} {insert} {buyfirst}'),
            'buttons' => [
                'ignore' => function ($url, $model, $key) {
                    return Html::a('<span class="fa fa-ban fa-fw" tooltip="Ignore?"></span>', '/newgameexp/?status=ignore&sid=' . $model->id . ($_GET['search'] ? "&search=" . $_GET['search'] : '') . ($_GET['page'] ? '&page=' . $_GET['page'] : '') . ($_GET['sort'] ? '&sort=' . $_GET['sort'] : '') . ($_GET['filter'] ? '&filter=' . $_GET['filter'] : ''), ['class' => 'btn  btn-primary btn-xs']);
                },
                'postpone' => function ($url, $model, $key) {
                    return Html::a('<span class="fa fa-clock fa-fw" tooltip="Later?"></span>', '/newgameexp/?status=postpone&sid=' . $model->id . ($_GET['search'] ? "&search=" . $_GET['search'] : '') . ($_GET['page'] ? '&page=' . $_GET['page'] : '') . ($_GET['sort'] ? '&sort=' . $_GET['sort'] : '') . ($_GET['filter'] ? '&filter=' . $_GET['filter'] : ''), ['class' => 'btn  btn-primary btn-xs']);
                },
                'review' => function ($url, $model, $key) {
                    return Html::a('<span class="fa fa-question fa-fw" tooltip="Review?"></span>', '/newgameexp/?status=review&sid=' . $model->id . ($_GET['search'] ? "&search=" . $_GET['search'] : '') . ($_GET['page'] ? '&page=' . $_GET['page'] : '') . ($_GET['sort'] ? '&sort=' . $_GET['sort'] : '') . ($_GET['filter'] ? '&filter=' . $_GET['filter'] : ''), ['class' => 'btn  btn-primary btn-xs']);
                },
                'processed' => function ($url, $model, $key) {
                    return Html::a('<span class="fa fa-check fa-fw" tooltip="Processed?"></span>', '/newgameexp/?status=precessed&sid=' . $model->id . ($_GET['search'] ? "&search=" . $_GET['search'] : '') . ($_GET['page'] ? '&page=' . $_GET['page'] : '') . ($_GET['sort'] ? '&sort=' . $_GET['sort'] : '') . ($_GET['filter'] ? '&filter=' . $_GET['filter'] : ''), ['class' => 'btn  btn-primary btn-xs']);
                },
                'print' => function ($url, $model, $key) {
                    return Html::a('<span class="fa fa-print fa-fw" tooltip="Print?"></span>', '/newgameexp/?status=pnp&sid=' . $model->id . ($_GET['search'] ? "&search=" . $_GET['search'] : '') . ($_GET['page'] ? '&page=' . $_GET['page'] : '') . ($_GET['sort'] ? '&sort=' . $_GET['sort'] : '') . ($_GET['filter'] ? '&filter=' . $_GET['filter'] : ''), ['class' => 'btn  btn-primary btn-xs']);
                },
                'fan' => function ($url, $model, $key) {
                    return Html::a('<span class="fa fa-user fa-fw" tooltip="Fan?"></span>', '/newgameexp/?status=fan&sid=' . $model->id . ($_GET['search'] ? "&search=" . $_GET['search'] : '') . ($_GET['page'] ? '&page=' . $_GET['page'] : '') . ($_GET['sort'] ? '&sort=' . $_GET['sort'] : '') . ($_GET['filter'] ? '&filter=' . $_GET['filter'] : ''), ['class' => 'btn  btn-primary btn-xs']);
                },
                'insert' => function ($url, $model, $key) {
                    return Html::a('<span class="fa fa-box fa-fw" tooltip="Insert?"></span>', '/newgameexp/?status=insert&sid=' . $model->id . ($_GET['search'] ? "&search=" . $_GET['search'] : '') . ($_GET['page'] ? '&page=' . $_GET['page'] : '') . ($_GET['sort'] ? '&sort=' . $_GET['sort'] : '') . ($_GET['filter'] ? '&filter=' . $_GET['filter'] : ''), ['class' => 'btn  btn-primary btn-xs']);
                },
                'buyfirst' => function ($url, $model, $key) {
                    return Html::a('<span class="fa fa-dollar-sign fa-fw" tooltip="Buy First?"></span>', '/newgameexp/?status=buy-first&sid=' . $model->id . ($_GET['search'] ? "&search=" . $_GET['search'] : '') . ($_GET['page'] ? '&page=' . $_GET['page'] : '') . ($_GET['sort'] ? '&sort=' . $_GET['sort'] : '') . ($_GET['filter'] ? '&filter=' . $_GET['filter'] : ''), ['class' => 'btn  btn-primary btn-xs']);
                },

            ],
        ],
        'buttons2' => [
            'class' => ActionColumn::className(),
            'template' => ('{bgg}'),
            'buttons' => [
                'bgg' => function ($url, $model, $key) {
                    return Html::a('<span class="fa fa-dice fa-fw"></span>', 'https://boardgamegeek.com/boardgame/' . $model->bgg_id, ['class' => 'btn  btn-primary btn-xs', 'target' => '_blank']);
                },
            ],
        ],
        'preview' => ['label' => '', 'format' => 'raw', 'value' => function ($game) {
            return '<img style="max-width: 200px; max-height: 200px" src="' . $game->image_url . '" />';
        }],
        'parent' => ['label' => 'Parent game', 'format' => 'raw', 'value' => function ($game) {
            return $game->parent ? '<a href="/games/' . $game->parent->id . '"><img style="max-width: 100px; max-height: 100px" src="https://cdn.aivis.lv/gallery/games/' . $game->parent->id . '.jpg" /><br /><span>Id: ' . $game->parent->id . ';<br /> Title: ' . $game->parent->title . '</span></a>' : 'No Parent';
        }]
    ],
]);

// $result = Yii::$app->getDb()->createCommand("SELECT count(`id`) as counted, `status` FROM `game_expansion_importer` GROUP BY `status`")->queryAll();

// var_dump($result);

?>
