<h1>Images</h1>
<a class="btn btn-w-m btn-primary" href="/photos?new">Add New Image</a>
<br/><br/>
<?
use \yii\data\ActiveDataProvider;
use \yii\grid\GridView;
use \yii\grid\ActionColumn;
use \yii\helpers\Html;

if ($album) {
    $query = \app\models\Images::find()
        ->where(['gid' => $album])
        ->orderBy(['order'=>SORT_DESC])
        ->joinWith('album');;
} elseif ($session) {
    $query = \app\models\Images::find()
        ->where(['session_id' => $session])
        ->orderBy(['order'=>SORT_DESC])
        ->joinWith('album');;
} elseif ($song) {
    $query = \app\models\Images::find()
        ->where(['song_id' => $session])
        ->orderBy(['order'=>SORT_DESC])
        ->joinWith('album');;
} else {
    $query = \app\models\Images::find()->orderBy(['id'=>SORT_DESC])->joinWith('album');;
}

$provider = new ActiveDataProvider([
    'query' => $query,
    'pagination' => [
        'pageSize' => 50,
    ]
]);

$provider = new ActiveDataProvider([
    'query' => $query,
]);

?>

<div>
    <?php
    echo \app\widgets\LinkPagerB4::widget([
        'pagination' => $provider->pagination,
    ]);
    ?>
</div>

<?
echo  \app\widgets\GridViewB4::widget([
    'dataProvider' => $provider,
    'layout' => "{summary}\n{items}",
    'columns' => [
        'id'=>['label'=>'Id','attribute'=>'id'],
        'title'=>['label'=>'Title','attribute'=>'title'],
        'preview'=>['label'=>'Preview', 'format'=>'raw', 'value'=>function($image) {
            return '<img src="https://cdn.aivis.lv/gallery/250/' . $image->id . '.' . $image->ext . '" />';
        }],
        'order'=>['label'=>'Order','attribute'=>'order'],
        'visible'=>['label'=>'Visible', 'format'=>'raw', 'value'=>function($album) {
            return $album->visible == 1 ? '<span style="color: #1e7e34" class="fa fa-eye fa-fw"></span>' : '<span style="color: #a91c19" class="fa fa-eye fa-fw"></span>';
        }],
        'hasSong'=>['label'=>'Has Song', 'format'=>'raw', 'value'=>function($album) {
            return intval($album->song_id) > 0 ? '<span style="color: #1e7e34" class="fa fa-eye fa-fw"></span>' : '<span style="color: #a91c19" class="fa fa-eye fa-fw"></span>';
        }],
        'gid'=>['label'=>'Album', 'value'=>function($image) {
            return $image->album->title;
        }],
        'buttons'=>[
            'class' => ActionColumn::className(),
            'template'=>('{update} {delete}'),
            'buttons'=>[
                'update' => function ($url, $model, $key) {return Html::a('<span class="fa fa-edit fa-fw"></span>', '/photos/' . $model->id, ['class'=>'btn  btn-primary btn-xs']);},
                'delete' => function ($url, $model, $key) {return Html::a('<span class="fa fa-trash fa-fw"></span>', '/photos/' . $model->id . "?delete", ['onclick'=>'return confirm("Really delete?");','class'=>'btn  btn-danger btn-xs']);},
            ],
        ]
    ],
]) ?>

