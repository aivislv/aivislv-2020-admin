<a class="btn btn-w-m btn-primary" href="/photos?new">Add New Image</a>
<?
$formInputs = [
    'image' => [
        'title' => 'Photo',
        'type'=> $item->isNewRecord ? 'upload' : 'showImage',
        'source' => 'gallery/500/' . $item->id . '.jpg',
        'accept'=>'image/*'
    ],
    'title'=>['title'=>'Title'],
    'visible'=>['title'=>'Is visible', 'type'=>'boolean'],
    'order'=>['title'=>'Order id', 'type'=>'integer'],
    'place'=>['title'=>'Place'],
    'persons'=>['title'=>'People'],
    'created'=>['title'=>'Created', 'type' => 'disabled'],
    'ext'=>['type'=>'hidden'],
    'time'=>['type'=>'hidden']
];

$formInputs['gid'] = ['title' => 'Album', 'type' => 'select', 'data' => \app\models\Albums::find()->orderBy(['id'=>SORT_DESC])->all(), 'key' => 'id', 'label' => 'title'];
$formInputs['session_id'] = ['title' => 'Session', 'type' => 'select', 'data' => \app\models\PhotoSession::find()->orderBy(['id'=>SORT_DESC])->all(), 'key' => 'id', 'label' => 'title'];
$formInputs['song_id'] = ['title' => 'Song', 'type' => 'select', 'data' => \app\models\Songs::find()->all(), 'key' => 'id', 'label' => 'title'];

echo $this->render('@app/views/admin/blank/edit', array('item'=>$item,'post'=>'photos', 'formInputs'=>$formInputs));