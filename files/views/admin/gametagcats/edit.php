<a class="btn btn-w-m btn-primary" href="/gametagcats?new">Add New Game Tag Category</a>
<?

use app\models\GameTagCategories;
use app\models\GameTags;

$selectedObj = $item->getGameTags()->all();

$formInputs = [
    'name' => ['title' => 'Tag'],
    'color' => ['title' => 'Color'],
];

// -- Games

$tags = GameTags::find()->where(['category_id' => $item->id])->orderBy(['tag' => SORT_ASC])->all();

$selected = [];

foreach ($selectedObj as $value) {
    $selected[$value->id] = true;
}

$dataFields = [];
$selecetField = [];

$formInputs['selectedGames'] = [
    'title' => 'Tags', 'type' => 'list', 'data' => $tags, 'row' => function ($data) {
        return '<a href="/gametags/' . $data->id . '">' . $data->tag . '</a>';
    }
];

echo $this->render('@app/views/admin/blank/edit', array('item' => $item, 'post' => 'gametagcats', 'formInputs' => $formInputs));