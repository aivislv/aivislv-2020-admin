<?

use app\models\GameTags;

$parent = $item->getParent()->one();

$formInputs = [
    'photo' => ['title' => 'Game image', 'source' => 'gallery/games/' . $item->id . '.jpg', 'type' => 'showImage'],
    'title' => ['title' => 'Title'],
    'expansions' => ['title' => '', 'type' => 'echo', 'text' => '<a class="btn btn-w-m btn-primary" href="/games/expansions/' . $item->id . '">View expansions</a>'],
    'parent' => ['title' => 'Parent game', 'type' => 'echo', 'text' => ($parent ? '<a href="/games/' . $parent->id . '">' . $parent->title . '</a>' : 'None')],
    'expansion' => ['title' => 'Parent game id', 'type' => 'integer'],
    'wish_weight' => ['title' => 'Wish Weight', 'type' => 'integer'],
    'skip_exp_check' => ['title' => 'Skip Expansion Check', 'type' => 'boolean'],
    'check_price' => ['title' => 'Check price', 'type' => 'boolean'],
    'is_accessory' => ['title' => 'Is Accessory', 'type' => 'boolean'],
    'is_promo' => ['title' => 'Is Promo', 'type' => 'boolean'],
    'min_players' => ['title' => 'Min Players', 'type' => 'integer'],
    'max_players' => ['title' => 'Max Players', 'type' => 'integer'],
    'players' => ['title' => 'Player counts', 'type' => 'echo', 'text' => $item->players],
    'bgg_id' => ['title' => 'BGG id', 'type' => 'disabled'],
    'plays' => ['title' => 'Total Plays', 'type' => 'disabled'],
    'rating' => ['title' => 'Rating', 'type' => 'disabled'],
    'box_min' => ['title' => 'Min Players', 'type' => 'disabled'],
    'box_max' => ['title' => 'Max Players', 'type' => 'disabled'],
    'min_playtime' => ['title' => 'Min playtime', 'type' => 'disabled'],
    'max_playtime' => ['title' => 'Max playtime', 'type' => 'disabled'],
    'playtime' => ['title' => 'Playtime', 'type' => 'disabled'],
    'avg_score' => ['title' => 'AVG score', 'type' => 'disabled'],
    'bye_score' => ['title' => 'BYE score', 'type' => 'disabled'],
    'usercount' => ['title' => 'User count', 'type' => 'disabled'],
    'wishlist_priority' => ['title' => 'Wishlist priority', 'type' => 'disabled'],
    'rank' => ['title' => 'Bgg Rank', 'type' => 'disabled'],
    'type' => ['title' => 'Type', 'type' => 'selectSimple', 'data' => [1 => 'Owned', 2 => 'Wishlist', 3 => 'Played']],
    'description' => ['title' => 'Description', 'type' => 'text'],
];

$tags = GameTags::find()->orderBy(['category_id' => SORT_ASC, 'tag' => SORT_ASC])->all();

$selectedObj = $item->getTags()->all();
$selected = [];
$selecetField = [];

foreach ($selectedObj as $value) {
    $selected[$value->id] = true;
}

foreach ($tags as $key => $tag) {
    $category = $tag->getCategory()->one();
    $dataFields[$key] = '
<span class="ms-selectedObject">
    <span>[' . $category->name . '] ' . $tag->tag . '</span>
</span>';

    if ($selected[$tag->id]) {
        $selecetField[$key] = $tag;
    }
}

$formInputs['selectedTags'] = [
    'title' => 'Game tags', 'type' => 'list', 'data' => $selecetField, 'row' => function ($data) {
        return $data->tag;
    }
];

$formInputs['tags'] = [
    'title' => 'Tags',
    'type' => 'multiselect',
    'view' => $dataFields,
    'data' => $tags,
    'value' => 'id',
    'selected' => $selected
];

echo $this->render('@app/views/admin/blank/edit', array('item'=>$item,'post'=>'games', 'formInputs'=>$formInputs));