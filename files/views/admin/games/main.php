<h1>Games</h1>
<!-- <a class="btn btn-w-m btn-primary" href="/games?new">Add New Game</a> -->
<?

use app\models\Games;
use app\widgets\GridViewB4;
use app\widgets\LinkPagerB4;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\grid\ActionColumn;
use yii\helpers\Html;

if ($game) {
    $parent = Games::findOne($game);

    if ($parent) {
        $query = Games::find()
            ->where(['games.expansion' => $parent->bgg_id])
            ->orderBy(['bgg_id' => SORT_ASC])
            ->joinWith('parent');
    } else {
        $query = Games::find()->joinWith('parent')->orderBy(['title' => SORT_ASC]);
    }
} else {
    if ($_GET['search']) {
        $query = Games::find()->where(['like', 'games.title', $_GET['search']])->joinWith('parent')->orderBy(['title' => SORT_ASC]);
    } else {
        if ($_GET['filter']) {
            $props = explode(',', $_GET['filter']);
            $query = Games::find()->where(['=', 'games.expansion', 0])->where(['=', 'games.expansion', $props[0]])->andWhere(['=', 'games.type', $props[1]])->joinWith('parent')->orderBy(['title' => SORT_ASC]);
        } else {
            $query = Games::find()->joinWith('parent')->orderBy(['title' => SORT_ASC]);
        }
    }
}

$provider = new ActiveDataProvider([
    'query' => $query,
]);

?>

<div>
    <div>
        <form method="get" action="/games">
            <div class="input-group mb-3" style="max-width: 300px;">
                <input placeholder="Search..." class="form-control"  name="search" value="<?=$_GET['search'];?>"/>
                <div class="input-group-append">
                    <button type="submit" class="btn btn-outline-primary">Search</button>
                    <button type="button" class="btn btn-outline-primary"
                            onclick="document.location.href = '/games?filter=0,1'">My games
                    </button>
                    <button type="button" class="btn btn-outline-primary"
                            onclick="document.location.href = '/games?filter=0,2'">Wish games
                    </button>
                </div>
            </div>
        </form>
    </div>
    <? if ($_GET['search']) { ?>
        <div class="alert alert-success" role="alert">
            Search results for: <strong><?=$_GET['search'];?></strong>
        </div>
    <? } ?>
    <br />
    <?php
    echo LinkPagerB4::widget([
        'pagination' => $provider->pagination,
    ]);
    ?>
</div>
<?
echo GridViewB4::widget([
    'dataProvider' => $provider,
    'layout' => "{summary}\n{items}",
    'columns' => [
        'id' => ['label' => 'Id', 'attribute' => 'id'],
        'title' => ['label' => 'Title', 'attribute' => 'title'],
        'preview' => ['label' => '', 'format' => 'raw', 'value' => function ($game) {
            return '<img src="https://cdn.aivis.lv/gallery/games/' . $game->id . '.jpg" />';
        }],
        'parent' => ['label' => 'Parent game', 'format' => 'raw', 'value' => function ($game) {
            return $game->parent ? '<a href="/games/' . $game->parent->id . '"><img style="max-width: 100px; max-height: 100px" src="https://cdn.aivis.lv/gallery/games/' . $game->parent->id . '.jpg" /><br /><span>Id: ' . $game->parent->id . ';<br /> Title: ' . $game->parent->title . '</span></a>' : 'No Parent';
        }],
        'owned'=>['label'=>'Status', 'format'=>'raw', 'value'=>function($game) {
            return $game->type === 1 ? 'Owned' : 'Wishlist';
        }],
        'buttons'=>[
            'class' => ActionColumn::className(),
            'template'=>('{update} {delete}'),
            'buttons'=>[
                'update' => function ($url, $model, $key) {return Html::a('<span class="fa fa-edit fa-fw"></span>', '/games/' . $model->id, ['class'=>'btn  btn-primary btn-xs']);},
                'delete' => function ($url, $model, $key) {return Html::a('<span class="fa fa-trash fa-fw"></span>', '/games/' . $model->id . "?delete", ['onclick'=>'return confirm("Really delete?");','class'=>'btn  btn-danger btn-xs']);},
            ],
        ]
    ],
]) ?>
