<h1>Users</h1>
    <a class="btn btn-w-m btn-primary" href="/users?new">Add New User</a>
<br/><br/>
<?
use \yii\data\ActiveDataProvider;
use app\models\Users;
use \yii\grid\GridView;
use \yii\grid\ActionColumn;
use \yii\helpers\Html;

$query = Users::find();

$provider = new ActiveDataProvider([
    'query' => $query,
]);

echo  \app\widgets\GridViewB4::widget([
    'dataProvider' => $provider,
    'columns' => [
        'id'=>['label'=>'Id','attribute'=>'id'],
        'username'=>['label'=>'Username','attribute'=>'username'],
        'email'=>['label'=>'Email','attribute'=>'email'],
        'buttons'=>[
            'class' => ActionColumn::className(),
            'template'=>('{update} {delete}'),
            'buttons'=>[
                //'view' => function ($url, $model, $key) {return Html::a('<span class="fa fa-eye fa-fw"></span>', '/admin/users/' . $model->id . "?view", ['class'=>'btn  btn-primary btn-xs']);},
                'update' => function ($url, $model, $key) {return Html::a('<span class="fa fa-edit fa-fw"></span>', '/users/' . $model->id, ['class'=>'btn  btn-primary btn-xs']);},
                'delete' => function ($url, $model, $key) {return Html::a('<span class="fa fa-trash fa-fw"></span>', '/users/' . $model->id . "?delete", ['onclick'=>'return confirm("Really delete?");','class'=>'btn  btn-danger btn-xs']);},
            ],
        ]
    ],
]) ?>
