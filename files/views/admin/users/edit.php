<?
$formInputs = [
    'username'=>['title'=>'Username'],
    'email'=>['title'=>'Email'],
    'password'=>['title'=>'Password','type'=>'password'],
    'name'=>['title'=>'Name'],
];

echo $this->render('@app/views/admin/blank/edit', array('item'=>$item,'post'=>'users', 'formInputs'=>$formInputs));