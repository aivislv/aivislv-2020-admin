<?
$formInputs = [
    'title'=>['title'=>'Title'],
    'visible'=>['title'=>'Is visible', 'type'=>'boolean'],
    'order'=>['title'=>'Order id', 'type'=>'integer'],
];

if (!empty($item->id)) {
    $formInputs['image'] = ['title' => 'Photo', 'type' => 'select', 'data' => \app\models\Images::find()->where(['gid' => $item->id])->all(), 'key' => 'id', 'value' => $item->image, 'label' => function($image, $props) {
        return 'ID: ' . $image['id'] . ' - ' . $image['title'] . '; ' . $image['persons'] . '; ' . $image['place'];
    }];
}

if (!empty($item->image)) {
    $formInputs['preview'] = [
        'title' => 'Title Photo',
        'type'=> 'showImage',
        'source' => 'gallery/500/' . $item->image . '.jpg'
    ];
}

echo '<a class="btn btn-w-m btn-primary" href="/photos/album/' . $item->id . '">View/Edit photos</a>';

echo $this->render('@app/views/admin/blank/edit', array('item'=>$item,'post'=>'albums', 'formInputs'=>$formInputs));