<h3>Reset cache <a class="btn btn-w-m btn-primary" href="http://crons.aivis.lv/?do=flush">DO!</a></h3>

<h4>Game imports</h4>
<a class="btn btn-w-m btn-primary" href="http://crons.aivis.lv/?do=games&action=base">Import games</a>
<br/>
<br/>
<a class="btn btn-w-m btn-primary" href="http://crons.aivis.lv/?do=games&action=base&list=wish">Import games,
    wishlist</a>
<br/>
<br/>
<a class="btn btn-w-m btn-primary" href="http://crons.aivis.lv/?do=games&action=base&list=played">Import games,
    played</a>
<br/>
<br/>
<a class="btn btn-w-m btn-primary" href="http://crons.aivis.lv/?do=games&action=base&list=accessories">Import game
    accessories</a>
<br/>
<br/>
<a class="btn btn-w-m btn-primary" href="http://crons.aivis.lv/?do=games&action=expansions">Find game new
    expansions</a>
<br/>
<br/>
<a class="btn btn-w-m btn-primary" href="http://crons.aivis.lv/?do=games&action=prices">Import game prices</a>
<br/>
<br/>
<a class="btn btn-w-m btn-secondary" href="http://crons.aivis.lv/?do=games&action=base&force=skipPlays">Import games,
    skip plays
    (fast!)</a>
<br/>
<br/>
<a class="btn btn-w-m btn-secondary" href="http://crons.aivis.lv/?do=games&action=base&force=reuploadImages">Re-import,
    all games,
    images</a>
<br/>
<br/>
<a class="btn btn-w-m btn-secondary" href="http://crons.aivis.lv/?do=games&action=base&force=refetchPlayerCount">Re-fetch,
    all player
    counts</a>
<br/>
<br/>
<a class="btn btn-w-m btn-secondary" href="http://crons.aivis.lv/?do=games&action=base&list=wish&force=reuploadImages">Import
    games, wishlist,
    all images</a>
<br/>
<br/>
<a class="btn btn-w-m btn-secondary" href="http://crons.aivis.lv/?do=games&help">Help!!!</a>
<br/><br/>
<h4>Book imports</h4>
<a class="btn btn-w-m btn-primary" href="http://crons.aivis.lv/?do=books&action=base">All goodreads books</a>
<br/>
<br/>
<a class="btn btn-w-m btn-secondary" href="http://crons.aivis.lv/?do=books&action=base&force=reuploadImages">All
    goodreads books, all images</a>
<br/>
<br/>
<a class="btn btn-w-m btn-secondary" href="http://crons.aivis.lv/?do=books&action=owned">All local books
    update</a> (All with tmp_id)
<br/>
<br/>
<a class="btn btn-w-m btn-secondary" href="http://crons.aivis.lv/?do=books&action=count">Recount author and series
    books</a>
<br/>
<br/>
<h4>Anime imports</h4>
<a class="btn btn-w-m btn-primary" href="http://crons.aivis.lv/?do=anime">Import all anime</a>
<br/>
<br/>
<a class="btn btn-w-m btn-primary" href="http://crons.aivis.lv/?do=anime&force=reuploadImages">Import all anime, all
    images</a>
<br/>
<br/>
<h4>Movie imports</h4>
<a class="btn btn-w-m btn-primary" href="http://crons.aivis.lv/?do=movies">Import watched movies</a>
<br/>
<br/>
<a class="btn btn-w-m btn-primary" href="http://crons.aivis.lv/?do=movies&list=wish">Import wishlist movies</a>
<br/>
<br/>
<a class="btn btn-w-m btn-secondary" href="http://crons.aivis.lv/?do=movies&force=reuploadImages">Import watched movies,
    all
    images</a>
<br/>
<br/>
<a class="btn btn-w-m btn-secondary" href="http://crons.aivis.lv/?do=movies&list=wish&force=reuploadImages">Import
    wishlist movies,
    all images</a>
<br/>
<br/>