<h1>Books wish</h1>
<?

use app\models\Books;
use app\models\Games;
use app\widgets\GridViewB4;
use app\widgets\LinkPagerB4;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\grid\ActionColumn;
use yii\helpers\Html;

$type = null;

if (isset($_GET['type'])) {
    $type = intval($_GET['type']);
}

$query = Books::find()->where(['>', 'id', 0]);

if (isset($_GET['search'])) {
    $query = $query->andWhere(['like', 'title', $_GET['search']]);
}

if ($type !== null) {
    $query = $query->andWhere(['=', 'isOwned', ($type == 1 ? 1 : 0)])->andWhere(['=', 'isRead', 0]);
}

$query = $query->orderBy(['wish_weight' => SORT_DESC, 'title' => SORT_ASC]);

$provider = new ActiveDataProvider([
    'query' => $query,
]);

?>

<div>
    <div>
        <form method="get" action="/books">
            <div class="input-group mb-3" style="max-width: 300px;">
                <input type="hidden" name="wish" value="1">
                <input placeholder="Search..." class="form-control" name="search" value="<?= $_GET['search']; ?>"/>
                <div class="input-group-append">
                    <button type="submit" class="btn btn-outline-primary">Search</button>
                    <button type="button" class="btn btn-outline-primary"
                            onclick="document.location.href = '/books?wish&type=1'">Owned
                    </button>
                    <button type="button" class="btn btn-outline-primary"
                            onclick="document.location.href = '/books?wish&type=2'">Wishlist
                    </button>
                </div>
            </div>
        </form>
    </div>
    <? if ($_GET['search']) { ?>
        <div class="alert alert-success" role="alert">
            Search results for: <strong><?= $_GET['search']; ?></strong>
        </div>
    <? } ?>
    <br/>
    <?php
    echo LinkPagerB4::widget([
        'pagination' => $provider->pagination,
    ]);
    ?>
</div>
<?
echo GridViewB4::widget([
    'dataProvider' => $provider,
    'layout' => "{summary}\n{items}",
    'columns' => [
        'id' => ['label' => 'Id', 'attribute' => 'id'],
        'preview' => ['label' => '', 'format' => 'raw', 'value' => function ($book) {
            return '<img src="https://cdn.aivis.lv/gallery/books/covers/' . $book->id . '.jpg" />';
        }],
        'isOwned' => ['label' => 'Is Owned', 'format' => 'raw', 'value' => function ($book) {
            return $book->isOwned == 1 ? '<span style="color: #1e7e34" class="fa fa-check fa-fw"></span>' : '<span style="color: #a91c19" class="fa fa-check fa-fw"></span>';
        }],
        'wish_weight' => ['label' => 'Weight', 'value' => 'wish_weight'],
        'buttons' => [
            'class' => ActionColumn::className(),
            'template' => ('{downBottom} {down100} {down50} {down10} {down5} {down1} {up1} {up5} {up10} {up50} {up100} {upTop1} {upTop2} {upTop3}  {update}'),
            'buttons' => [
                'up1' => function ($url, $model, $key) {
                    return Html::a('+1', '/books?wish&up=1&id=' . $model->id . (isset($_GET['type']) ? "&type=" . intval($_GET['type']) : ""), ['class' => 'btn  btn-primary btn-xs']);
                },
                'up5' => function ($url, $model, $key) {
                    return Html::a('+5', '/books?wish&up=5&id=' . $model->id . (isset($_GET['type']) ? "&type=" . intval($_GET['type']) : ""), ['class' => 'btn  btn-primary btn-xs']);
                },
                'up10' => function ($url, $model, $key) {
                    return Html::a('+10', '/books?wish&up=10&id=' . $model->id . (isset($_GET['type']) ? "&type=" . intval($_GET['type']) : ""), ['class' => 'btn  btn-primary btn-xs']);
                },
                'up50' => function ($url, $model, $key) {
                    return Html::a('+50', '/books?wish&up=50&id=' . $model->id . (isset($_GET['type']) ? "&type=" . intval($_GET['type']) : ""), ['class' => 'btn  btn-primary btn-xs']);
                },
                'up100' => function ($url, $model, $key) {
                    return Html::a('+100', '/books?wish&up=100&id=' . $model->id . (isset($_GET['type']) ? "&type=" . intval($_GET['type']) : ""), ['class' => 'btn  btn-primary btn-xs']);
                },
                'upTop1' => function ($url, $model, $key) {
                    return Html::a('Top1', '/books?wish&set=T1&id=' . $model->id . (isset($_GET['type']) ? "&type=" . intval($_GET['type']) : ""), ['class' => 'btn  btn-primary btn-xs']);
                },
                'upTop2' => function ($url, $model, $key) {
                    return Html::a('Top2', '/books?wish&set=T2&id=' . $model->id . (isset($_GET['type']) ? "&type=" . intval($_GET['type']) : ""), ['class' => 'btn  btn-primary btn-xs']);
                },
                'upTop3' => function ($url, $model, $key) {
                    return Html::a('Top3', '/books?wish&set=T3&id=' . $model->id . (isset($_GET['type']) ? "&type=" . intval($_GET['type']) : ""), ['class' => 'btn  btn-primary btn-xs']);
                },

                'down1' => function ($url, $model, $key) {
                    return Html::a('-1', '/books?wish&up=-1&id=' . $model->id . (isset($_GET['type']) ? "&type=" . intval($_GET['type']) : ""), ['class' => 'btn  btn-primary btn-xs']);
                },
                'down5' => function ($url, $model, $key) {
                    return Html::a('-5', '/books?wish&up=-5&id=' . $model->id . (isset($_GET['type']) ? "&type=" . intval($_GET['type']) : ""), ['class' => 'btn  btn-primary btn-xs']);
                },
                'down10' => function ($url, $model, $key) {
                    return Html::a('-10', '/books?wish&up=-10&id=' . $model->id . (isset($_GET['type']) ? "&type=" . intval($_GET['type']) : ""), ['class' => 'btn  btn-primary btn-xs']);
                },
                'down50' => function ($url, $model, $key) {
                    return Html::a('-50', '/books?wish&up=-50&id=' . $model->id . (isset($_GET['type']) ? "&type=" . intval($_GET['type']) : ""), ['class' => 'btn  btn-primary btn-xs']);
                },
                'down100' => function ($url, $model, $key) {
                    return Html::a('-100', '/books?wish&up=-100&id=' . $model->id . (isset($_GET['type']) ? "&type=" . intval($_GET['type']) : ""), ['class' => 'btn  btn-primary btn-xs']);
                },
                'downBottom' => function ($url, $model, $key) {
                    return Html::a('Bottom', '/books?wish&set=B&id=' . $model->id . (isset($_GET['type']) ? "&type=" . intval($_GET['type']) : ""), ['class' => 'btn  btn-primary btn-xs']);
                },

                'update' => function ($url, $model, $key) {
                    return Html::a('<span class="fa fa-edit fa-fw"></span>', '/books/' . $model->id, ['class' => 'btn  btn-primary btn-xs']);
                },
            ],
        ]
    ],
]) ?>
