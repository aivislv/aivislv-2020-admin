<a class="btn btn-w-m btn-primary" href="/books?new">Add New Book</a>
<?

use app\models\BookAuthors;
use app\models\BookSeries;

$formInputs = [
    'image_link' => ['title' => 'Upload image', 'type' => 'upload', 'accept' => 'image/*'],
    'title' => ['title' => 'Title'],
    'goodreads_id' => ['title' => 'Goodreads id', 'type' => 'integer'],
    'goodreads_work_id' => ['title' => 'Goodreads work id', 'type' => 'integer'],
    'rating' => ['title' => 'Rating', 'type' => 'integer'],
    'avg_rating' => ['title' => 'Average Rating', 'type' => 'float'],
    'ISBN13' => ['title' => 'ISBN13'],
    'ISBN10' => ['title' => 'ISBN10'],
    'wish_weight' => ['title' => 'Wish Weight', 'type' => 'integer'],
    'description' => ['title' => 'Description', 'type' => 'text'],
    'skipupdatetitle' => ['title' => 'Skip update title', 'type' => 'boolean'],
    'visible' => ['title' => 'Visible', 'type' => 'boolean'],
    'isOwned' => ['title' => 'Is Owned', 'type' => 'boolean'],
    'isRead' => ['title' => 'Is Read', 'type' => 'selectSimple', 'data' => [
        '0' => 'No',
        '1' => 'Reading',
        '2' => 'Yes',
    ]],

    'created' => ['title' => 'Created', 'type' => 'disabled']
];

// -- Authors

$authors = BookAuthors::find()->orderBy(['name_eng' => SORT_ASC])->all();
$selectedObj = $item->getBookAuthors()->all();

$selected = [];

foreach ($selectedObj as $value) {
    $selected[$value->id] = true;
}

$dataFields = [];
$selectedAuthors = [];

foreach ($authors as $key => $author) {
    $dataFields[$key] = '
<span class="ms-selectedObject">
    <span>' . $author->name_eng . '</span>
</span>';

    if ($selected[$author->id]) {
        $selectedAuthors[$key] = $author;
    }
}

$formInputs['selectedAuthor'] = [
    'title' => 'Selected Authors', 'type' => 'list', 'data' => $selectedAuthors, 'row' => function ($data) {
        return '<a href="/authors/' . $data->id . '">' . $data->name_eng . '</a>';
    }
];

$formInputs['authors'] = [
    'title' => 'Author',
    'type' => 'multiselect',
    'view' => $dataFields,
    'data' => $authors,
    'value' => 'id',
    'selected' => $selected
];

// -- Series

$series = BookSeries::find()->orderBy(['title' => SORT_ASC])->all();
$selectedObj = $item->getBookSeries()->all();

$selected = [];

foreach ($selectedObj as $value) {
    $selected[$value->id] = true;
}

$seriesDataFields = [];
$selectedSeries = [];

foreach ($series as $key => $serie) {
    if ($serie->title !== "") {
        $seriesDataFields[$key] = '
<span class="ms-selectedObject">
    <span>' . $serie->title . '</span>
</span>';
    }

    if ($selected[$serie->id]) {
        $selectedSeries[$key] = $serie;
    }
}

$formInputs['selectedSeries'] = [
    'title' => 'Selected Series', 'type' => 'list', 'data' => $selectedSeries, 'row' => function ($data) {
        return '<a href="/series/' . $data->id . '">' . $data->title . '</a>';
    }
];

$formInputs['series'] = [
    'title' => 'Series',
    'type' => 'multiselect',
    'view' => $seriesDataFields,
    'data' => $series,
    'value' => 'id',
    'selected' => $selected
];

if (!$item->isNewRecord) {
    $formInputs['preview'] = ['title' => 'Image', 'type' => 'showImage', 'source' => "gallery/books/covers/" . $item->id . ".jpg"];
}

echo $this->render('@app/views/admin/blank/edit', array('item' => $item, 'post' => 'books', 'formInputs' => $formInputs));