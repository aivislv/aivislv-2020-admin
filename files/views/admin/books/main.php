<h1>Books</h1>
<a class="btn btn-w-m btn-primary" href="/books?new">Add New Book</a>
<br/><br/>
<!-- <a class="btn btn-w-m btn-primary" href="/games?new">Add New Game</a> -->
<?

use app\models\Books;
use app\widgets\GridViewB4;
use app\widgets\LinkPagerB4;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\grid\ActionColumn;
use yii\helpers\Html;

if ($_GET['search']) {
    $query = Books::find()->where(['like', 'title', $_GET['search']]);
} else {
    $query = Books::find();
}


$provider = new ActiveDataProvider([
    'query' => $query,
]);

?>

<div>
    <div>
        <form method="get" action="/books">
            <div class="input-group mb-3" style="max-width: 300px;">
                <input placeholder="Search..." class="form-control" name="search" value="<?= $_GET['search']; ?>"/>
                <div class="input-group-append">
                    <button type="submit" class="btn btn-outline-primary">Search</button>
                </div>
            </div>
        </form>
    </div>
    <? if ($_GET['search']) { ?>
        <div class="alert alert-success" role="alert">
            Search results for: <strong><?= $_GET['search']; ?></strong>
        </div>
    <? } ?>
    <br/>
    <?php
    echo LinkPagerB4::widget([
        'pagination' => $provider->pagination,
    ]);
    ?>
</div>
<?
echo GridViewB4::widget([
    'dataProvider' => $provider,
    'layout' => "{summary}\n{items}",
    'columns' => [
        'id' => ['label' => 'Id', 'attribute' => 'id'],
        'title' => ['label' => 'Title', 'attribute' => 'title'],
        'preview' => ['label' => '', 'format' => 'raw', 'value' => function ($book) {
            return '<img src="https://cdn.aivis.lv/gallery/books/covers/' . $book->id . '.jpg" />';
        }],
        'isRead' => ['label' => 'Is Read', 'format' => 'raw', 'value' => function ($book) {
            return $book->isRead === 2 ? '<span style="color: #1e7e34" class="fa fa-check fa-fw"></span>' : (
            $book->isRead === 1 ? '<span style="color: #ffb800" class="fa fa-book-open fa-fw"></span>' : '<span style="color: #a91c19" class="fa fa-check fa-fw"></span>'
            );
        }],
        'isOwned' => ['label' => 'Is Owned', 'format' => 'raw', 'value' => function ($book) {
            return $book->isOwned == 1 ? '<span style="color: #1e7e34" class="fa fa-check fa-fw"></span>' : '<span style="color: #a91c19" class="fa fa-check fa-fw"></span>';
        }],
        'buttons' => [
            'class' => ActionColumn::className(),
            'template' => ('{owned} {update} {delete}'),
            'buttons' => [
                'owned' => function ($url, $model, $key) {
                    return Html::a('<span class="fa fa-book fa-fw" tooltip="Is Owned?"></span>', '/books/?owned=' . $model->id . ($_GET['search'] ? "&search=" . $_GET['search'] : '') . ($_GET['page'] ? '&page=' . $_GET['page'] : '') . ($_GET['sort'] ? '&sort=' . $_GET['sort'] : ''), ['class' => 'btn  btn-primary btn-xs']);
                },
                'update' => function ($url, $model, $key) {
                    return Html::a('<span class="fa fa-edit fa-fw"></span>', '/books/' . $model->id, ['class' => 'btn  btn-primary btn-xs']);
                },
                'delete' => function ($url, $model, $key) {
                    return Html::a('<span class="fa fa-trash fa-fw"></span>', '/books/' . $model->id . "?delete", ['onclick' => 'return confirm("Really delete?");', 'class' => 'btn  btn-danger btn-xs']);
                },
            ],
        ]
    ],
]) ?>
