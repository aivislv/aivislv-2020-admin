<?php

namespace app\controllers;

use app\models\About;
use app\models\Albums;
use app\models\Animes;
use app\models\BookAuthors;
use app\models\Books;
use app\models\BookSeries;
use app\models\GameExpansionImporter;
use app\models\GamePrices;
use app\models\Games;
use app\models\GameTagCategories;
use app\models\GameTags;
use app\models\Images;
use app\models\Movies;
use app\models\PhotoSession;
use app\models\Songs;
use Imagine\Gd\Image;
use Yii;
use yii\web\Controller;
use app\models\Users;
use yii\web\View;

class AdminController extends Controller
{
    var $layout = 'admin';

    public function beforeAction($action) {
        Yii::$app->view->registerCssFile('https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');
        Yii::$app->view->registerCssFile('https://use.fontawesome.com/releases/v5.7.2/css/all.css');
        Yii::$app->view->registerCssFile('/css/multi-list.css');

        Yii::$app->view->registerCssFile('/css/admin.css?' . filemtime('css/admin.css'));


        Yii::$app->view->registerJsFile('https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js', ['position'=>View::POS_HEAD]);
        Yii::$app->view->registerJsFile('https://cdn.ckeditor.com/ckeditor5/11.2.0/classic/ckeditor.js', ['position'=>View::POS_HEAD]);
        Yii::$app->view->registerJsFile('/js/multi-list.js', ['position'=>View::POS_HEAD]);
        Yii::$app->view->registerJsFile('/js/std.js', ['position' => View::POS_HEAD]);

        if (Yii::$app->user->isGuest && $action->id != 'login') {
            $this->redirect('/login');
            return;
        } else {
            Yii::$app->breadcrumbs->add($action->id, '/' . $action->id);
            return parent::beforeAction($action);
        }
    }

    public function renderStandardView($action, $id = false, $subAction = false) {
        $available = [
            'users' => ['actions' => ['new', 'edit', 'delete'], 'model' => new Users()],
            'albums' => ['actions' => ['new', 'edit', 'delete'], 'model' => new Albums()],
            'sessions' => ['actions' => ['new', 'edit', 'delete', 'multiupload'], 'model' => new PhotoSession()],
            'songs' => ['actions' => ['new', 'edit', 'delete'], 'model' => new Songs()],
            'photos' => ['actions' => ['new', 'edit', 'delete', 'album', 'session', 'song'], 'model' => new Images()],
            'games' => ['actions' => ['edit', 'delete', 'expansions', 'wish'], 'model' => new Games()],
            'books' => ['actions' => ['edit', 'delete', 'new', 'wish'], 'model' => new Books()],
            'authors' => ['actions' => ['edit', 'delete', 'new'], 'model' => new BookAuthors()],
            'series' => ['actions' => ['edit', 'delete', 'new'], 'model' => new BookSeries()],
            'anime' => ['actions' => ['edit', 'delete', 'wish'], 'model' => new Animes()],
            'movies' => ['actions' => ['edit', 'delete'], 'model' => new Movies()],
            'content' => ['actions' => ['new', 'edit'], 'model' => new About()],
            'gametagcats' => ['actions' => ['new', 'edit', 'delete'], 'model' => new GameTagCategories()],
            'gametags' => ['actions' => ['new', 'edit', 'delete'], 'model' => new GameTags()],
            'gameprices' => ['actions' => ['edit', 'list'], 'model' => new GamePrices(), 'addedModel' => new Games()],
            'newgameexp' => ['actions' => ['edit'], 'model' => new GameExpansionImporter()],
            'crons' => ['actions' => []],
        ];

        if (isset($available[$action])) {

            if (isset($available[$action]['model']) && isset($available[$action]['model']->primary)) {
                $primary = $available[$action]['model']->primary;
            } else {
                $primary = 'id';
            }

            if (isset($_GET['delete'])) {
                if (in_array('delete', $available[$action]['actions'])) {
                    $actual = $available[$action]['model']::find()->where([$primary => $id])->one();
                    if ($actual) {
                        $to = $actual->delete();
                        if ($to) {
                            $this->redirect($to);
                        } else {
                            $this->redirect('/' . $action);
                        }
                        return;
                    }
                }
            }

            if (isset($_GET['main'])) {
                if (in_array('view',$available[$action]['actions'])) {
                    $actual = $available[$action]['model']::find()->where([$primary=>$id])->one();
                    if ($actual) {
                        return $actual->setMain();
                    }
                }
            }

            if (isset($_GET['wish'])) {
                if (in_array('wish', $available[$action]['actions'])) {
                    if (isset($_GET['id'])) {
                        $actual = $available[$action]['model']::find()->where([$primary => intval($_GET['id'])])->one();
                        if ($actual) {
                            $actual->changeWish();
                            return $this->render($action . '/wish', ['item' => intval($_GET['id']), 'link' => $action]);
                        }
                    }

                    return $this->render($action . '/wish', ['item' => false, 'link' => $action]);
                }
            }

            if (isset($_GET['view'])) {
                if (in_array('view',$available[$action]['actions'])) {
                    $actual = $available[$action]['model']::find()->where([$primary=>$id])->one();
                    if ($actual) {
                        Yii::$app->breadcrumbs->add('View ID: ' . $actual->id, '/' . $action . '/' . $id);
                        return $this->render($action . '/view', ['item'=>$actual, 'link'=>$action]);
                    }
                }
            }

            if (isset($_GET['new'])) {
                Yii::$app->breadcrumbs->add('Add new', '/' . $action . '?new');
                if (in_array('new',$available[$action]['actions'])) {
                    $actual = new $available[$action]['model']();
                    if ($actual) {
                        if (isset($_POST['save'])) {
                            $actual = $actual->adminSave();
                            $err = $actual->getErrors();
                            if (empty($err)) {
                                $this->redirect('/' . $action . "/" . $actual->{$primary});
                            }
                        }
                        return $this->render($action . '/edit', ['item' => $actual, 'link' => $action]);
                    }
                }
            }

            if (isset($_GET['list'])) {
                if (in_array('list', $available[$action]['actions'])) {
                    $actual = $available[$action]['addedModel']::find()->where([$primary => intval($_GET['list'])])->one();
                    if ($actual) {
                        return $this->render($action . '/list', ['item' => $actual, 'link' => $action]);
                    }
                }
            }

            if (isset($_GET['multiupload'])) {
                if (in_array('multiupload', $available[$action]['actions'])) {
                    return $this->render($action . '/multiupload', ['link' => $action, 'session' => $id]);
                }
            }

            if (isset($_GET['upload'])) {
                if (in_array('upload', $available[$action]['actions'])) {
                    $actual = $available[$action]['model']::find()->where([$primary => $id])->one();
                    if ($actual) {
                        $this->layout = 'ajax';
                        $response = $actual->uploadItems();
                        return $this->render('@app/views/admin/blank/ajax', ['response' => $response, 'link' => $action]);
                    }
                }
            }

            if (isset($subAction) && $subAction === 'album') {
                if (in_array('album', $available[$action]['actions'])) {

                    return $this->render($action . '/main', ['link'=>$action, 'album' => $id]);
                }
            }

            if (isset($subAction) && $subAction === 'session') {
                if (in_array('session', $available[$action]['actions'])) {

                    return $this->render($action . '/main', ['link'=>$action, 'session' => $id]);
                }
            }

            if (isset($subAction) && $subAction === 'song') {
                if (in_array('song', $available[$action]['actions'])) {

                    return $this->render($action . '/main', ['link'=>$action, 'song' => $id]);
                }
            }

            if (isset($subAction) && $subAction === 'expansions') {
                if (in_array('expansions', $available[$action]['actions'])) {

                    return $this->render($action . '/main', ['link'=>$action, 'game' => $id]);
                }
            }

            if ($id!==false) {
                if (in_array('edit',$available[$action]['actions'])) {
                    $actual = $available[$action]['model']::find()->where([$primary=>$id])->one();
                    if ($actual) {
                        Yii::$app->breadcrumbs->add('Edit ID: ' . $actual->id, '/' . $action . '/' . $id);
                        if (isset($_POST['save'])) {
                            $actual->adminSave();
                        }
                        return $this->render($action . '/edit', ['item'=>$actual, 'link'=>$action]);
                    }
                }
            }

            return $this->render($action . '/main', ['link'=>$action]);
        }

        $this->redirect('/admin/');
        return;
    }

    public function actionIndex()
    {
        return $this->render('index', ['title' => 'Test']);
    }

    public function actionUsers($id = false)
    {
        return $this->renderStandardView('users', $id);
    }

    public function actionAlbums($id = false)
    {
        return $this->renderStandardView('albums', $id);
    }

    public function actionSessions($id = false)
    {
        return $this->renderStandardView('sessions', $id);
    }

    public function actionSongs($id = false)
    {
        return $this->renderStandardView('songs', $id);
    }

    public function actionAnimes($id = false)
    {
        return $this->renderStandardView('anime', $id);
    }

    public function actionBooks($id = false)
    {
        if ($_GET['owned']) {
            $book = Books::find()->where(['id' => intval($_GET['owned'])])->one();
            if ($book) {
                $book->isOwned = $book->isOwned === 1 ? 0 : 1;
                $book->save();
            }
        }

        return $this->renderStandardView('books', $id);
    }

    public function actionAuthors($id = false)
    {
        return $this->renderStandardView('authors', $id);
    }

    public function actionSeries($id = false)
    {
        return $this->renderStandardView('series', $id);
    }


    public function actionMovies($id = false)
    {
        return $this->renderStandardView('movies', $id);
    }

    public function actionCrons()
    {
        return $this->renderStandardView('crons', false);
    }

    public function actionContent($id = false)
    {
        return $this->renderStandardView('content', $id);
    }

    public function actionGames($subAction = false, $id = false)
    {
        if (!$id) {
            return $this->renderStandardView('games', $subAction);
        } else {
            return $this->renderStandardView('games', $id, $subAction);
        }
    }

    public function actionGameprices($subAction = false, $id = false)
    {
        if ($_GET['status']) {
            $game = GamePrices::findOne(intval($_GET['id']));
            if ($game) {
                $game->status = $_GET['status'];
                $game->save();
            }
        }

        if (!$id) {
            return $this->renderStandardView('gameprices', $subAction);
        } else {
            return $this->renderStandardView('gameprices', $id, $subAction);
        }
    }

    public function actionGametagcats($subAction = false, $id = false)
    {
        if (!$id) {
            return $this->renderStandardView('gametagcats', $subAction);
        } else {
            return $this->renderStandardView('gametagcats', $id, $subAction);
        }
    }

    public function actionNewgameexp($subAction = false, $id = false)
    {
        if ($_GET['status']) {
            $game = GameExpansionImporter::find()->where(['id' => intval($_GET['sid'])])->one();
            if ($game) {
                $game->status = $_GET['status'];
                $game->save();
            }
        }

        if (!$id) {
            return $this->renderStandardView('newgameexp', $subAction);
        } else {
            return $this->renderStandardView('newgameexp', $id, $subAction);
        }
    }

    public function actionGametags($subAction = false, $id = false)
    {
        if (!$id) {
            return $this->renderStandardView('gametags', $subAction);
        } else {
            return $this->renderStandardView('gametags', $id, $subAction);
        }
    }

    public function actionPhotos($subAction = false, $id = false)
    {
        if (!$id) {
            return $this->renderStandardView('photos', $subAction);
        } else {
            return $this->renderStandardView('photos', $id, $subAction);
        }
    }

    public function actionLogin() {

        if (isset($_GET['login'])) {
            if (Users::login()) {
                $this->redirect('/');
                return;
            }
        }

        $this->layout='admin.blank.php';

        return $this->render('login');
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->render('login');
    }
}
