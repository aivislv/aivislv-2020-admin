<?php

namespace app\controllers;

use app\models\Images;
use Yii;
use yii\web\Controller;
use yii\web\UnauthorizedHttpException;
use yii\web\View;

class AjaxController extends Controller
{
    var $layout = 'ajax';

    public function beforeAction($action) {

        if (Yii::$app->user->isGuest && $action->id!='login') {
            throw new UnauthorizedHttpException();
        } else {
            Yii::$app->controller->enableCsrfValidation = false;
            return parent::beforeAction($action);
        }
    }

    public function actionBlank($id = 1)
    {
        Yii::$app->response->headers->add('Content-Type', 'application/json');
        $result = "Blank response";
        Yii::$app->response->data = json_encode($result);

    }

    public function actionSession()
    {
        Yii::$app->response->headers->add('Content-Type', 'application/json');
        $image = new Images();
        $result = $image->sessionUpload();
        Yii::$app->response->data = json_encode($result);

    }
}
