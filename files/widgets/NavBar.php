<?php
namespace app\widgets;

class NavBar
{
    static public function render($listNew, $path = '/', $children = '') { ?>
     <nav class="navbar navbar-expand-md navbar-light bg-light" role="navigation" style="font-size:18px;">
            <div class="container-fluid">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Collect the nav links, forms, and other content for toggling -->

                    <ul class="navbar-nav mr-auto">
                        <?

                        foreach ($listNew as $k => $v) {
                            //if (\app\helpers\Rules::can($k)) {
                                if (!is_array($v)) {
                                    echo '<li ' . ($k == \Yii::$app->controller->action->id ? 'class="nav-item active"' : 'class="nav-item"') . '><a class="nav-link" href="' . $path . $k . '">' . $v . '</a></li>';
                                } else {
                                    ?>
                                        <li class="nav-item dropdown" role="presentation" >
                                            <a class="nav-link dropdown-toggle" href="#" id="navBar<?=$k;?>" role="button" aria-haspopup="true" data-toggle="dropdown" aria-expanded="false"><?=$v['title'];?></a>
                                            <div class="dropdown-menu" aria-labelledby="navBar<?=$k;?>">
                                                <? foreach ($v['sub'] as $key => $item) {
                                                    echo '<a ' . ($key == \Yii::$app->controller->action->id ? 'class="dropdown-item active"' : 'class="dropdown-item"') . ' href="' . $path . $key . '">' . $item . '</a>';
                                                } ?>
                                            </div>
                                        </li>
                                    <?
                                }
                            //}
                        }
                        ?>
                        <li class="nav-item"><a class="nav-link" href="/logout">Logout</a></li>
                    </ul>
                    <?=$children;?>
                </div>
            </div><!-- /.container-fluid -->
        </nav>
    <? }
}
