<?php
namespace app\widgets;

class GridViewB4 extends \yii\grid\GridView
{
    static function widget($config = []) {
        $config['layout'] = "{summary}\n{items}";

        $widget = parent::widget($config);

        return GridViewB4::wrap($widget, $config);
    }

    static function wrap($widget, $config) {

        $pager = LinkPagerB4::widget([
            'pagination' => $config['dataProvider']->pagination,
        ]);

        return $pager . $widget . $pager;

    }
}
