<?php
namespace app\widgets;

use yii\widgets\LinkPager;
use yii\helpers\Html;

class LinkPagerB4 extends LinkPager
{
    public $firstPageLabel = '<i class="fas fa-fast-backward"></i>';
    public $lastPageLabel = '<i class="fas fa-fast-forward"></i>';
    public $prevPageLabel = '<i class="fas fa-caret-left"></i>';
    public $nextPageLabel = '<i class="fas fa-caret-right"></i>';
    public $maxButtonCount = '7';

    public $options = [ 'tag' => 'ul', 'class' => 'pagination' ];

    // Customzing CSS class for pager link
    public $linkOptions = [ 'class' => 'page-link' ];
    public $disabledPageCssClass = 'page-item disabled';
    public $pageCssClass = 'page-item';

    // Customzing CSS class for navigating link
    public $prevPageCssClass = 'page-item';
    public $nextPageCssClass = 'page-item';
    public $firstPageCssClass = 'page-item';
    public $lastPageCssClass = 'page-item';
    public $disabledListItemSubTagOptions = [ 'class' => 'page-link' ];

    protected function renderPageButtons() {
        $result = parent::renderPageButtons();

        return Html::tag('nav', $result, ['aria-label'=>'Backer pages']);
    }
}
