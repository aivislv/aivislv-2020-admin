var ___STD_CLASS = false;

var __STD_XPN = function () {
};

var ___STD_CLASS_CORE = function () {

    var p = this;
    p.IE = navigator.userAgent.indexOf("MSIE") != -1;

    p._ = function (elm, params) {

        var p = this;

        if (params == false) {
            if (typeof (elm) != 'object') {
                return document.getElementById(elm)
            } else {
                if (p.IE) {
                    __STD_XPN(elm)
                } else {
                    elm
                }
            }
        } else if (elm != false) {

            var m = document.createElement(elm);
            for (var a in params) {
                switch (a) {
                    case "style":
                        var b;
                        for (b in params[a]) {
                            if (b == "float") {
                                m.style['styleFloat'] = m.style['cssFloat'] = params['float'][b];
                            } else if ('transform') {
                                m.style['transform'] = m.style['-ms-transform'] = m.style['-moz-transform'] = m.style['-o-transform'] = m.style['-webkit-transform'] = params['transform'][b];
                            } else {
                                m.style[b] = params[a][b];
                            }
                        }
                        break;
                    case "apc":
                        p.apc.apply(m, [params[a]]);
                        break;
                    case "app":
                        p.apc.apply(params[a], [m]);
                        break;
                    default:
                        m[a] = params[a];
                }
            }
            m.apc = p.apc;
            m.transform = p.transform;
            if (p.IE) {
                __STD_XPN(m);
            }
            return m;
        } else {
            return p;
        }
    };

    p.apc = function (nod) {
        if (typeof (nod) == 'undefined' || nod == null || nod == 'null' || nod == "") {
            nod = "";
        }
        var z = typeof (nod);
        if (z == "string" || z == 'number' || z == 'function') {
            this.appendChild(document.createTextNode(nod));
        } else if (nod instanceof Array) {
            for (var a in nod) {
                var za = typeof (nod[a]);
                if (za == "string" || za == 'number') {
                    this.appendChild(document.createTextNode(nod[a]));
                } else {
                    if (za == "object") {
                        this.appendChild(nod[a]);
                    }
                }
            }
        } else {
            this.appendChild(nod);
        }
        return this;
    };

    p.transform = function (applyThis) {
        this.style['transform'] = this.style['-ms-transform'] = this.style['-moz-transform'] = this.style['-o-transform'] = this.style['-webkit-transform'] = applyThis;
    };

    p.method = {

        'formatTime': function (d) {

            var seconds = d % 60;
            if (seconds < 10) {
                seconds = "0" + seconds
            }
            var min = Math.floor(d / 60);
            var minutes = min % 60;
            if (minutes < 10) {
                minutes = "0" + minutes
            }
            min = Math.floor(min / 60);
            var hours = min;
            if (hours < 10) {
                hours = "0" + hours
            }

            return hours + ":" + minutes + ":" + seconds;

        },

        'con': function (d, strict) {
            if (typeof (console) != 'undefined') {
                console.info(d);
            } else {
                if (typeof (strict) != 'undefined' && strict == true) {
                    alert(d);
                }
            }
        },

        'sval': function (to, from) {

            for (var a in from) {
                to[a] = from[a];
            }

        },
        'nval': function (to, from) {

            var newTo = {};

            for (var a in to) {
                newTo[a] = to[a];
            }

            for (var a in from) {
                newTo[a] = from[a];
            }

            return newTo;

        },

        isTouch: function () {
            return 'ontouchstart' in window // works on most browsers
                || 'onmsgesturechange' in window; // works on ie10
        },

        'getRandomElements': function (list, count, unique, excludeList) {

            if (typeof (unique) == 'undefined') {
                unique = true;
            }

            if (typeof (excludeList) == 'undefined') {
                excludeList = false;
            }

            var tmpList = [];

            for (var a = 0; a < list.length; a++) {
                tmpList.push(list[a]);
            }

            var tmpResultList = [];
            var tmpRandElement;

            for (var b = 0; b < count; b++) {
                var isAccepted = false;
                while (!isAccepted) {
                    isAccepted = true;
                    tmpRandElement = Math.round(Math.random() * (tmpList.length - 1));

                    if (excludeList.length !== false) {
                        for (var c = 0; c < excludeList.length; c++) {
                            if (excludeList[c] === tmpList[tmpRandElement]) {
                                isAccepted = false;
                            }
                        }
                    }

                    if (isAccepted) {
                        tmpResultList.push(tmpList[tmpRandElement]);
                    }
                }

                if (unique) {
                    tmpList.splice(tmpRandElement, 1);
                }

            }

            return tmpResultList;

        },

        'shuffle': function (input) {

            var subsort = [];
            for (var a = 0; a < input.length; a++) {
                subsort.push(input[a]);
            }

            var i = subsort.length;
            var k = subsort.length;

            while (--i) {
                var j = Math.floor(Math.random() * (k - 1));
                var temp = subsort[i];
                subsort[i] = subsort[j];
                subsort[j] = temp;
            }

            return subsort;

        },

        'initTemplate': function () {
            if (p.templateStarted === void (0)) {
                mf.Template.init();
                p.templateStarted = true;
            }
        },

        'Template': function (screen, fx, variables, container) {

            if (container === void (0)) {
                container = frame.config.displayContainerRef
            }

            frame.loadedsCall[screen] = fx;
            _.initTemplate();
            var myTpl = mf.Template.create(screen, null, variables);
            myTpl.show(container);

            return myTpl;

        },

        'getChoice': function (list) {

            var compare = [];
            var totalPercents = 0;

            for (var a in list) {

                if (list[a] > 0) {
                    totalPercents = totalPercents + list[a];

                    compare.push(
                        {
                            'resp': a,
                            'var': totalPercents
                        }
                    );
                }

            }

            var choice = Math.random() * totalPercents;

            for (var b = 0; b < compare.length; b++) {
                if (compare[b]['var'] >= choice) {
                    return compare[b]['resp'];
                }
            }

        }
        /*
         'getRelativeTop':function (element, relative) {
         var my = element;
         var elementList = [element];
         while (my.parentNode) {
         my = my.parentNode;
         if (my.style.position==='absolute' || my.style.position==='relative' || my === document.body) {
         elementList.push(my);
         }
         }

         var my = relative;
         var relativeList = [relative];
         while (my.parentNode) {
         my = my.parentNode;
         if (my.style.position==='absolute' || my.style.position==='relative' || my === document.body) {
         relativeList.push(my);
         }
         }

         var relativeFound = false;

         for (var a = 0; a<elementList.length; a++) {

         for (var b = 0; b<relativeList.length; b++) {
         if (relativeList[b]===elementList[a] && relativeFound===false) {
         relativeFound = elementList[a];
         }
         }

         }

         var position;

         var elementPosition = [0,0];

         my = elementList[0];

         while (my!==relativeFound) {
         position = my.elements
         }

         }
         */

    };

};

_ = function (par1, par2) {

    if (!___STD_CLASS) {

        ___STD_CLASS = new ___STD_CLASS_CORE();
        for (var a in ___STD_CLASS.method) {
            _[a] = ___STD_CLASS.method[a];
        }

    }

    if (typeof (par1) != 'undefined') {
        if (typeof (par2) == 'undefined') {
            par2 = false
        }
        return ___STD_CLASS._(par1, par2);
    } else {
        return ___STD_CLASS.IE;
    }

};

var Clock = function (d) {

    var p = this;

    var data = {};

    var startTimer = function () {
        data.myTime = 0;

        if (config.direction == 'up') {
            config.updateCall(_.formatTime(data.myTime));
        } else {
            config.updateCall(_.formatTime(config.time - data.myTime));
        }

        data.timer = new InnerTimer({
            'time': config.time * 1000,
            'step': config.step,
            'callBack': finishTime,
            'execute': updateTimer,
            'mustStop': config.mustStop
        })
    };

    var updateTimer = function () {

        data.myTime++;

        if (config.direction == 'up') {
            config.updateCall(_.formatTime(data.myTime));
        } else {
            config.updateCall(_.formatTime(config.time - data.myTime));
        }
        config.execute();

        return true;
    };

    var finishTime = function () {

        if (config.direction == 'up') {
            config.updateCall(_.formatTime(data.time));
        } else {
            config.updateCall(_.formatTime(0));
        }

        config.callBack();

    };

    p.stop = function () {
        data.timer.stop();
    };

    var config = {
        'time': 60, //seconds
        'step': 1000, //microseconds
        'direction': 'down', //direction for counter - up - from zero to time, down - from time to zero
        'updateCall': frame.header.setTime, //call function for timer update
        'execute': function () {
        },
        'callBack': function () {
        },
        'mustStop': function () {
        }
    };

    _.sval(config, d);

    startTimer();

};

var InnerTimer = function (d) {

    var p = this;

    var data = {};

    var create = function () {

        data.counter = 0;

        if (config.onInit(config.forward)) {
            data.timeout = setInterval(counter, config.step)
        }

    };

    var counter = function () {

        data.counter = data.counter + config.step;

        if (data.counter >= config.time && config.time !== false) {
            clearInterval(data.timeout);
            config.callBack(config.forward);
        } else {
            var result = config.execute(config.forward);
            if (!result) {
                clearInterval(data.timeout);
            }
        }

    };

    p.stop = function () {
        clearInterval(data.timeout);
        config.mustStop();
    };

    var config = {
        onInit: function () {
            return true;
        },
        callBack: function () {
            return true;
        },
        execute: function () {
            return true;
        },
        time: 1000,
        step: 100,
        mustStop: function () {
            return false;
        },
        forward: {}
    };

    if (typeof (d) != 'undefined') {
        _.sval(config, d);
    }

    config.forward['_timer'] = p;

    create();
};

window.requestAnimFrame = (function () {
    return window.requestAnimationFrame ||
        window.webkitRequestAnimationFrame ||
        window.mozRequestAnimationFrame ||
        window.oRequestAnimationFrame ||
        window.msRequestAnimationFrame ||
        function (callback) {
            window.setTimeout(callback, 1000 / 60);
        };
})();

var InnerAnimator = function (d) {

    var p = this;

    var data = {};

    var create = function () {

        data.counter = 0;

        if (config.onInit(config.forward)) {
            data.timeout = requestAnimFrame(counter)
        }

    };

    var counter = function () {

        data.counter = data.counter + config.step;

        if (data.counter >= config.time && config.time !== false) {
            clearInterval(data.timeout);
            config.callBack(config.forward);
        } else {
            var result = config.execute(config.forward);
            if (!result) {
                //  clearInterval(data.timeout);
            } else {
                requestAnimFrame(counter)
            }
        }

    };

    p.stop = function () {
        clearInterval(data.timeout);
        config.mustStop();
    };

    var config = {
        onInit: function () {
            return true;
        },
        callBack: function () {
            return true;
        },
        execute: function () {
            return true;
        },
        time: 1000,
        step: 100,
        mustStop: function () {
            return false;
        },
        forward: {}
    };

    if (typeof (d) != 'undefined') {
        _.sval(config, d);
    }

    config.forward['_timer'] = p;

    create();
};

_();