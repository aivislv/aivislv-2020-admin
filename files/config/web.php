<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'name'=> 'aivis.lv',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => '_PmsTggAVs#QN!Af2vCTzgMsQ49U^J',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\Users',
            'enableAutoLogin' => true,
        ],
        'r' => [
            'class' => 'app\widgets\Help',
        ],
        'store' => [
            'class' => 'app\helpers\S3',
        ],
        'auth0' => [
            'class' => 'app\helpers\Auth0',
        ],
        'breadcrumbs' => [
            'class' => 'app\helpers\BreadcrumbsHelper',
            []
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'codemix\streamlog\Target',
                    'url' => 'php://stdout',
                    'levels' => ['error', 'warning'],
                    'logVars' => [],
                ],
            ],
        ],

        'db' => $db,
        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'normalizer' => [
                'class' => 'yii\web\UrlNormalizer',
                // use temporary redirection instead of permanent for debugging
                // 'action' => UrlNormalizer::ACTION_REDIRECT_TEMPORARY,
            ],
            'rules' => [
                '/'=>'admin/index',
                'admin'=>'admin/index',
                '<action:\w+>'=>'admin/<action>',
                '<action:\w+>/<id:(\w+)-(\w+)>'=>'admin/<action>',
                '<action:\w+>/<id:\d+>'=>'admin/<action>',
                '<action:\w+>/<subAction:\w+>/<id:\d+>'=>'admin/<action>',
                'ajax/<action:\w+>/<id:\d+>'=>'ajax/<action>',
                'ajax/<action:\w+>/<pid:\d+>/user/<id:\d+>'=>'ajax/<action>',
                'ajax/<action:\w+>/<pid:\d+>/item/<id:\d+>'=>'ajax/<action>',
            ],
        ],
        /*
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
        */
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        'allowedIPs' => ['127.0.0.1', '::1', '172.18.0.*', '172.19.0.10'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        'allowedIPs' => ['127.0.0.1', '::1', '172.18.0.*', '*', '172.19.0.10'],
    ];
}

return $config;
