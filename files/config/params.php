<?php

return [
    'adminEmail' => 'admin@example.com',
    'domain'=>'http://admin.aivis.lv',
    'clientDomain'=>'http://aivis.lv',
    'auth0' => [
        'clientId'=>getenv('AUTH_ID'),
        'clientSecret'=>getenv('AUTH_SECRET'),
        'clientDomain'=>getenv('AUTH_DOMAIN'),
    ],
    'breadcrumbs' => [],
];
