<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=' . getenv('MYSQL_HOST') . ';dbname=' . getenv('MYSQL_DB') . ';port=' . getenv('MYSQL_PORT'),
    'username' => getenv('MYSQL_USER'),
    'password' => getenv('MYSQL_PASS'),
    'charset' => 'utf8',

    // Schema cache options (for production environment)
    'enableSchemaCache' => !YII_ENV_DEV,
    'schemaCacheDuration' => 60,
    //'schemaCache' => 'cache',
];
