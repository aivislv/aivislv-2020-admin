<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "games2tags".
 *
 * @property int $game_id
 * @property int $tag_id
 *
 * @property Games $game
 * @property GameTags $tag
 */
class Games2tags extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'games2tags';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['game_id', 'tag_id'], 'required'],
            [['game_id', 'tag_id'], 'integer'],
            [['game_id', 'tag_id'], 'unique', 'targetAttribute' => ['game_id', 'tag_id']],
            [['tag_id'], 'exist', 'skipOnError' => true, 'targetClass' => GameTags::className(), 'targetAttribute' => ['tag_id' => 'id']],
            [['game_id'], 'exist', 'skipOnError' => true, 'targetClass' => Games::className(), 'targetAttribute' => ['game_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'game_id' => 'Game ID',
            'tag_id' => 'Tag ID',
        ];
    }

    /**
     * Gets query for [[Game]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getGame()
    {
        return $this->hasOne(Games::className(), ['id' => 'game_id']);
    }

    /**
     * Gets query for [[Tag]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTag()
    {
        return $this->hasOne(GameTags::className(), ['id' => 'tag_id']);
    }
}
