<?php

namespace app\models;

use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "game_prices".
 *
 * @property int $id
 * @property int $source_id
 * @property string $title
 * @property string $link
 * @property float $price
 * @property string $created
 * @property string $status
 * @property string $source
 * @property int $item_id
 * @property string $details
 * @property string $condition
 * @property string $currency
 * @property string $date
 * @property int $available
 * @property string|null $country
 * @property float|null $shipping
 *
 * @property Games $item
 */
class GamePrices extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'game_prices';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['source_id', 'link', 'price', 'source', 'item_id', 'details', 'condition', 'currency', 'date'], 'required'],
            [['source_id', 'item_id', 'available'], 'integer'],
            [['price', 'shipping'], 'number'],
            [['created', 'date'], 'safe'],
            [['details'], 'string'],
            [['title'], 'string', 'max' => 100],
            [['link'], 'string', 'max' => 255],
            [['status'], 'string', 'max' => 15],
            [['source', 'currency', 'country'], 'string', 'max' => 5],
            [['condition'], 'string', 'max' => 30],
            [['source_id', 'source'], 'unique', 'targetAttribute' => ['source_id', 'source']],
            [['item_id'], 'exist', 'skipOnError' => true, 'targetClass' => Games::className(), 'targetAttribute' => ['item_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'source_id' => 'Source ID',
            'title' => 'Title',
            'link' => 'Link',
            'price' => 'Price',
            'created' => 'Created',
            'status' => 'Status',
            'source' => 'Source',
            'item_id' => 'Item ID',
            'details' => 'Details',
            'condition' => 'Condition',
            'currency' => 'Currency',
            'date' => 'Date',
            'available' => 'Available',
            'country' => 'Country',
            'shipping' => 'Shipping',
        ];
    }

    /**
     * Gets query for [[Item]].
     *
     * @return ActiveQuery
     */
    public function getItem()
    {
        return $this->hasOne(Games::className(), ['id' => 'item_id']);
    }
}
