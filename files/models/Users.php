<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "users".
 *
 * @property int $id
 * @property string $username
 * @property int $status
 * @property int $company_id
 * @property string $password
 * @property string $name
 * @property string $email
 *
 */
class Users extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username', 'password', 'name', 'email'], 'required'],
            [['username'], 'string', 'max' => 20],
            [['password', 'email'], 'string', 'max' => 255],
            [['name'], 'string', 'max' => 50],
            [['username'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'password' => 'Password',
            'name' => 'Name',
            'email' => 'Email',
        ];
    }

    static public function login()
    {
        $user = Users::find()->where(['username'=>$_POST['username']])->one();

        if (!empty($user)) {

            if (password_verify($_POST['password'], $user->password)) {
                Yii::$app->user->login($user, 0);
                return true;
            }
        }

        return false;
    }

    function adminSave() {
        $errors = false;

        $fieldsToSave = array(
            'username','password'=>'password','email', 'name'
        );

        if ($this->isNewRecord) {
            $product = new Users();
            $new = true;
        } else {
            $product = $this;
            $new = false;
        }

        foreach ($fieldsToSave as $field => $k) {
            if ($k=='bool') {
                $product->{$field} = isset($_POST[$field])&&$_POST[$field]=='on'?'1':'0';
            } elseif ($k=='list') {
                if (isset($_POST[$field])) {
                    $product->{$field} = implode(";", $_POST[$field]);
                } else {
                    $product->{$field} = '';
                }

            } else if ($k=='password') {
                if (isset($_POST[$field]) && $_POST[$field]!='') {
                    $product->password = password_hash($_POST['password'], PASSWORD_DEFAULT);
                }
            } else {
                $product->{$k} = $_POST[$k];
            }
        }

        $product->validate();
        $errors = $product->getErrors();
        if (empty($errors)) {
            $product->save();
        }
        return $product;

    }


    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        return false; //static::findOne(['access_token' => $token]);
    }

    public function getId()
    {
        return $this->id;
    }

    public function getAuthKey()
    {
        return true; // $this->authKey;
    }

    public function validateAuthKey($authKey)
    {
        return true; //$this->authKey === $authKey;
    }
}
