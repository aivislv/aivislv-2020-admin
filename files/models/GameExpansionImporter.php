<?php

namespace app\models;

use Yii;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "game_expansion_importer".
 *
 * @property int $id
 * @property int $bgg_id
 * @property int|null $item_id
 * @property int $parent_id
 * @property string $status
 * @property string $title
 * @property string $image_url
 * @property string $created
 * @property string $type
 * @property string $description
 *
 * @property Games $parent
 * @property Games $item
 */
class GameExpansionImporter extends ExtendedActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'game_expansion_importer';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['bgg_id', 'parent_id', 'title'], 'required'],
            [['bgg_id', 'item_id', 'parent_id'], 'integer'],
            [['image_url', 'description'], 'string'],
            [['created'], 'safe'],
            [['status'], 'string', 'max' => 10],
            [['title'], 'string', 'max' => 255],
            [['type'], 'string', 'max' => 15],
            [['parent_id'], 'exist', 'skipOnError' => true, 'targetClass' => Games::className(), 'targetAttribute' => ['parent_id' => 'id']],
            [['item_id'], 'exist', 'skipOnError' => true, 'targetClass' => Games::className(), 'targetAttribute' => ['item_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'bgg_id' => 'Bgg ID',
            'item_id' => 'Item ID',
            'parent_id' => 'Parent ID',
            'status' => 'Status',
            'title' => 'Title',
            'image_url' => 'Image Url',
            'created' => 'Created',
            'type' => 'Type',
            'description' => 'Description',
        ];
    }

    /**
     * Gets query for [[Parent]].
     *
     * @return ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(Games::className(), ['id' => 'parent_id']);
    }

    /**
     * Gets query for [[Item]].
     *
     * @return ActiveQuery
     */
    public function getItem()
    {
        return $this->hasOne(Games::className(), ['id' => 'item_id']);
    }
}
