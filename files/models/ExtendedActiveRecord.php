<?php

namespace app\models;

use Aws\S3\S3Client;
use Exception;
use Yii;
use yii\db\ActiveRecord;

class ExtendedActiveRecord extends ActiveRecord
{

    function adminSave($fieldsToSave = [], $formData = [])
    {
        $errors = false;

        /*
            $fieldsToSave = [
                'simpleText',
                'someBoolean'=>'bool',
                'concatableList'=>'list',
                'password'=>'password',
                'fixedValue'=>['type'=>'fixed', 'value'=>'setValue'],
                'imageLink'=>['type'=>'file', 'path'=>'savePathFormat', 'filename'=>'fileNameFormat' 'fill'=>['title'=>1, 'someTitle'=>2], 'skipSave'=>false],
                'default'=>['type'=>'default', 'value'=>'setValue'], // sets only in no value present
                'error'=>['error'=>'value'],
                // somePath/#title#/#someTitle#_#id#.#mime#
                // reserved: field (field name), id (id of model), mime (mimetype)
            ];
         */

        if (empty($formData)) {
            $formData = $_POST;
        }

        if ($this->isNewRecord) {
            $modelClass = get_class($this);
            $model = new $modelClass;
            $new = true;
        } else {
            $model = $this;
            $new = false;
        }

        foreach ($fieldsToSave as $field => $k) {
            if ($k == 'bool') {
                $model->{$field} = isset($formData[$field]) && $formData[$field] == 'on' ? '1' : '0';
            } elseif ($k == 'list') {
                if (isset($formData[$field])) {
                    $model->{$field} = implode(";", $formData[$field]);
                } else {
                    $model->{$field} = '';
                }

            } elseif ($k == 'password') {
                if (isset($formData[$field]) && $formData[$field] != '') {
                    $model->password = password_hash($formData['password'], PASSWORD_DEFAULT);
                }
            } elseif (is_array($k) && $k['type'] == 'file') {
                // Do Nothing for now
            } elseif (is_array($k) && $k['type'] == 'fixed') {
                $model->{$field} = $k['value'];
            } elseif (is_array($k) && $k['type'] == 'error') {
                $model->addError($field, $k['error']);
            } elseif (is_array($k) && $k['type'] === 'default') {
                $setValue = $k['value'];

                if ($formData[$field]) {
                    $setValue = $formData[$field];
                }

                $model->{$field} = $setValue;
            } else {
                $model->{$k} = $formData[$k];
            }
        }

        $errors = $model->getErrors();

        if (empty($errors)) {
            $model->validate();
            $errors = $model->getErrors();
        }

        if (empty($errors)) {
            $model->save();

            foreach ($fieldsToSave as $field => $value) {
                if (is_array($value) && $value['type'] === "file") {
                    if (!empty($_FILES[$field]['name'])) {
                        $file = $this->uploadItem($model, $field, $value);

                        if (!$value['skipSave']) {
                            $model->{$field} = $file;
                        }
                    }
                }
            }

            $model->save();
        } else {
            var_dump($errors = $model->getErrors());
        }

        return $model;
    }

       function uploadItem($model, $field, $value) {
        try {
            $mimeList = array(
                'image/jpeg' => 'jpg',
                'image/gif' => 'gif',
                'image/png' => 'png',
                'image/tiff' => 'tif',
                'image/vnd.wap.wbmp' => 'wbmp',
                'image/bmp' => 'bmp'
            );

            $fill = $value['fill'];
            $fill['id'] = $model->id;
            $fill['mime'] = $mimeList[$_FILES[$field]['type']];
            $fill['field'] = $field;
            $fill['name'] = $_FILES[$field]['name'];

            $destination = $this->fill($fill, $value['path']);

            $name = '';

            if (!empty($_FILES[$field]['name'])) {
                $name = $this->fill($fill, $value['filename']);
                // copy($_FILES[$field]['tmp_name'], $destination . $name);

                $resultName = preg_replace('#/+#', '/', $destination . $name);

                Yii::$app->store->pushFile($_FILES[$field]['tmp_name'], $resultName, $_FILES[$field]['type']);
            }

            return $resultName;
        } catch (Exception $e) {
            return null;
        }
    }

    function fill($fill, $path) {
        $search = [];
        $replace = [];

        foreach ($fill as $key => $value) {
            $search[] = '#' . $key . '#';
            $replace[] = $value;
        }

        return str_replace($search, $replace, $path);
    }
}
