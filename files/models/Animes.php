<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "animes".
 *
 * @property int $id
 * @property int $mal_id
 * @property string $title
 * @property int $rating
 * @property int $status
 * @property string $image
 * @property int $updated
 * @property float $score
 * @property string $start_date
 * @property string $end_date
 * @property string $synopsis
 */
class Animes extends ExtendedActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'animes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['mal_id', 'title', 'rating', 'image', 'updated', 'score', 'start_date', 'end_date'], 'required'],
            [['mal_id', 'rating', 'status', 'updated', 'wish_weight'], 'integer'],
            [['score'], 'number'],
            [['synopsis'], 'string'],
            [['title', 'image'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mal_id' => 'Mal ID',
            'title' => 'Title',
            'rating' => 'Rating',
            'status' => 'Status',
            'image' => 'Image',
            'updated' => 'Updated',
            'score' => 'Score',
            'start_date' => 'Start Date',
            'end_date' => 'End Date',
            'synopsis' => 'Synopsis',
        ];
    }

    public function adminSave($fieldsToSave = [], $formData = [])
    {
        $fieldsToSave = array(
            'title',
            'status',
            'wish_weight'
        );
        return parent::adminSave($fieldsToSave); // TODO: Change the autogenerated stub
    }

    public function changeWish()
    {
        if (isset($_GET['up'])) {
            if ($_GET['up'] < 500 || $_GET['up'] > -500) {
                $this->wish_weight = $this->wish_weight + intval($_GET['up']);
            }
        }

        if (isset($_GET['set'])) {
            if ($_GET['set'] === "T1") {
                $maxWish = Animes::find()
                    ->where(['status' => $this->status])
                    ->orderBy(['wish_weight' => SORT_DESC])
                    ->one();

                $this->wish_weight = $maxWish->wish_weight + 1;
            }

            if ($_GET['set'] === "T2") {
                $maxWish = Animes::find()
                    ->where(['status' => $this->status])
                    ->orderBy(['wish_weight' => SORT_DESC])
                    ->one();

                $this->wish_weight = $maxWish->wish_weight;
                $maxWish->wish_weight = $maxWish->wish_weight + 1;
                $maxWish->save();
            }

            if ($_GET['set'] === "T3") {
                $maxWish = Animes::find()
                    ->where(['status' => $this->status])
                    ->orderBy(['wish_weight' => SORT_DESC])
                    ->limit(2)
                    ->all();

                $this->wish_weight = $maxWish[1]->wish_weight;
                $maxWish[1]->wish_weight = $maxWish[0]->wish_weight;
                $maxWish[0]->wish_weight = $maxWish[0]->wish_weight + 1;
                $maxWish[0]->save();
                $maxWish[1]->save();
            }

            if ($_GET['set'] === "B") {
                $maxWish = Animes::find()
                    ->where(['status' => $this->status])
                    ->orderBy(['wish_weight' => SORT_ASC])
                    ->one();

                $this->wish_weight = $maxWish->wish_weight - 1;
            }
        }

        $this->save();
    }
}
