<?php

namespace app\models;

use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "book2series".
 *
 * @property int $book_id
 * @property int $series_id
 * @property float|null $ordered
 * @property float|null $last_ordered
 *
 * @property Books $book
 * @property BookSeries $series
 */
class Book2series extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'book2series';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['book_id', 'series_id'], 'required'],
            [['book_id', 'series_id'], 'integer'],
            [['ordered', 'last_ordered'], 'number'],
            [['book_id'], 'exist', 'skipOnError' => true, 'targetClass' => Books::className(), 'targetAttribute' => ['book_id' => 'id']],
            [['series_id'], 'exist', 'skipOnError' => true, 'targetClass' => BookSeries::className(), 'targetAttribute' => ['series_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'book_id' => 'Book ID',
            'series_id' => 'Series ID',
            'ordered' => 'Ordered',
            'last_ordered' => 'Last Ordered',
        ];
    }

    /**
     * Gets query for [[Book]].
     *
     * @return ActiveQuery
     */
    public function getBook()
    {
        return $this->hasOne(Books::className(), ['id' => 'book_id']);
    }

    /**
     * Gets query for [[Series]].
     *
     * @return ActiveQuery
     */
    public function getSeries()
    {
        return $this->hasOne(BookSeries::className(), ['id' => 'series_id']);
    }
}
