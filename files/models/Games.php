<?php

namespace app\models;

use Yii;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "games".
 *
 * @property int $id
 * @property int $bgg_id
 * @property int $plays
 * @property float $rating
 * @property int|null $expansion
 * @property string $title
 * @property string $description
 * @property int $group_parent_id
 * @property int $min_players
 * @property int $max_players
 * @property int $min_playtime
 * @property int $max_playtime
 * @property int $playtime
 * @property float $avg_score
 * @property float $bye_score
 * @property int $usercount
 * @property int $rank
 * @property int $type
 * @property int|null $box_min
 * @property int|null $box_max
 * @property string $players
 * @property int $is_accessory
 * @property int $skip_exp_check
 * @property int $check_price
 *
 * @property GameExpansionImporter[] $gameExpansionImporters
 * @property GameExpansionImporter[] $gameExpansionImporters0
 * @property GamePrices[] $gamePrices
 * @property Games2tags[] $games2tags
 * @property GameTags[] $tags
 */
class Games extends ExtendedActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'games';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['bgg_id', 'plays', 'rating', 'title', 'description', 'group_parent_id', 'min_players', 'max_players', 'min_playtime', 'max_playtime', 'playtime', 'avg_score', 'bye_score', 'usercount', 'rank'], 'required'],
            [['bgg_id', 'plays', 'expansion', 'group_parent_id', 'min_players', 'max_players', 'min_playtime', 'max_playtime', 'playtime', 'usercount', 'rank', 'type', 'box_min', 'box_max', 'is_accessory', 'skip_exp_check', 'check_price', 'wish_weight', 'is_promo'], 'integer'],
            [['rating', 'avg_score', 'bye_score'], 'number'],
            [['description'], 'string'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'bgg_id' => 'Bgg ID',
            'plays' => 'Plays',
            'rating' => 'Rating',
            'expansion' => 'Expansion',
            'title' => 'Title',
            'description' => 'Description',
            'group_parent_id' => 'Group Parent ID',
            'min_players' => 'Min Players',
            'max_players' => 'Max Players',
            'min_playtime' => 'Min Playtime',
            'max_playtime' => 'Max Playtime',
            'playtime' => 'Playtime',
            'avg_score' => 'Avg Score',
            'bye_score' => 'Bye Score',
            'usercount' => 'Usercount',
            'rank' => 'Rank',
            'type' => 'Type',
            'box_min' => 'Box Min',
            'box_max' => 'Box Max',
            'players' => 'Players',
            'is_accessory' => 'Is Accessory',
            'skip_exp_check' => 'Skip Exp Check',
            'check_price' => 'Check Price',
        ];
    }

    /**
     * Gets query for [[GameExpansionImporters]].
     *
     * @return ActiveQuery
     */
    public function getGameExpansionImporters()
    {
        return $this->hasMany(GameExpansionImporter::className(), ['parent_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPlays()
    {
        return $this->hasMany(GamesPlays::className(), ['game_id' => 'id']);
    }

    /**
     * Gets query for [[GameExpansionImporters0]].
     *
     * @return ActiveQuery
     */
    public function getGameExpansionImporters0()
    {
        return $this->hasMany(GameExpansionImporter::className(), ['item_id' => 'id']);
    }

    /**
     * Gets query for [[GamePrices]].
     *
     * @return ActiveQuery
     */
    public function getGamePrices()
    {
        return $this->hasMany(GamePrices::className(), ['item_id' => 'id']);
    }

    /**
     * Gets query for [[Games2tags]].
     *
     * @return ActiveQuery
     */
    public function getGames2tags()
    {
        return $this->hasMany(Games2tags::className(), ['game_id' => 'id']);
    }


    /**
     * @return ActiveQuery
     */
    public function getExpansions()
    {
        return $this->hasMany(Games::className(), ['expansion' => 'bgg_id'])->from(['expansion' => Games::tableName()]);
    }

    /**
     * Gets query for [[Tags]].
     *
     * @return ActiveQuery
     */
    public function getTags()
    {
        return $this->hasMany(GameTags::className(), ['id' => 'tag_id'])->viaTable('games2tags', ['game_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(Games::className(), ['bgg_id' => 'expansion'])->from(['parent' => Games::tableName()]);
    }

    /**
     * @return ActiveQuery
     */
    public function getGroupParent()
    {
        return $this->hasOne(Games::className(), ['bgg_id' => 'group_parent_id'])->from(['groupParent' => Games::tableName()]);
    }

    public function adminSave($fieldsToSave = [], $formData = [])
    {
        $fieldsToSave = array(
            'title',
            'description',
            'type',
            'expansion',
            'skip_exp_check' => 'bool',
            'check_price' => 'bool',
            'is_accessory' => 'bool',
            'is_promo' => 'bool',
            'min_players',
            'max_players',
            'wish_weight'
        );

        $newSaved = parent::adminSave($fieldsToSave); // TODO: Change the autogenerated stub

        if (isset($_POST['tags']))
        {
            $newSaved->unlinkAll('tags', true);

            foreach($_POST['tags'] as $key => $item) {
                $tag = GameTags::find()->where(['id'=>$item])->one();
                $newSaved->link('tags', $tag);
            }
        }

        return $newSaved;
    }

    public function changeWish()
    {
        if (isset($_GET['up'])) {
            if ($_GET['up'] < 500 || $_GET['up'] > -500) {
                $this->wish_weight = $this->wish_weight + intval($_GET['up']);
            }
        }

        if (isset($_GET['set'])) {
            if ($_GET['set'] === "T1") {
                $maxWish = Games::find()
                    ->where(['type' => $this->type])
                    ->orderBy(['wish_weight' => SORT_DESC])
                    ->one();

                $this->wish_weight = $maxWish->wish_weight + 1;
            }

            if ($_GET['set'] === "T2") {
                $maxWish = Games::find()
                    ->where(['type' => $this->type])
                    ->orderBy(['wish_weight' => SORT_DESC])
                    ->one();

                $this->wish_weight = $maxWish->wish_weight;
                $maxWish->wish_weight = $maxWish->wish_weight + 1;
                $maxWish->save();
            }

            if ($_GET['set'] === "T3") {
                $maxWish = Games::find()
                    ->where(['type' => $this->type])
                    ->orderBy(['wish_weight' => SORT_DESC])
                    ->limit(2)
                    ->all();

                $this->wish_weight = $maxWish[1]->wish_weight;
                $maxWish[1]->wish_weight = $maxWish[0]->wish_weight;
                $maxWish[0]->wish_weight = $maxWish[0]->wish_weight + 1;
                $maxWish[0]->save();
                $maxWish[1]->save();
            }

            if ($_GET['set'] === "B") {
                $maxWish = Games::find()
                    ->where(['type' => $this->type])
                    ->orderBy(['wish_weight' => SORT_ASC])
                    ->one();

                $this->wish_weight = $maxWish->wish_weight - 1;
            }
        }

        $this->save();
    }
}
