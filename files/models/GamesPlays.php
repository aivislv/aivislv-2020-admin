<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "games_plays".
 *
 * @property int $id
 * @property int|null $game_id
 * @property string $date
 * @property int $quantity
 * @property int $bgg_play_id
 */
class GamesPlays extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'games_plays';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['game_id', 'quantity', 'bgg_play_id'], 'integer'],
            [['date', 'quantity', 'bgg_play_id'], 'required'],
            [['date'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'game_id' => 'Game ID',
            'date' => 'Date',
            'quantity' => 'Quantity',
            'bgg_play_id' => 'Bgg Play ID',
        ];
    }
}
