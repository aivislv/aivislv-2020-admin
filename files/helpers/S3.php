<?php
namespace app\helpers;

use Aws\S3\S3Client;
use Exception;
use Yii;
use Aws;

class S3
{
    private $connection = false;

    function __construct()
    {
        $this->connection = new S3Client([
            'version' => 'latest',
            'region' => getenv('AWS_REGION'),
            'credentials' => [
                'key' => getenv('AWS_ACCESS_KEY_ID'), //'AKIAVPFDGBCY3ZRE5UXC',
                'secret' => getenv('AWS_SECRET_ACCESS_KEY'), //'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',
            ]
        ]);
    }

    function pushFile($source, $filename, $type)
    {
        try {
            $result = $this->connection->putObject([
                'Bucket' => getenv('S3_BUCKET'),
                'Key' => $filename,
                'SourceFile' => $source,
                'ContentType' => $type,
                'CacheControl' => 'public, max-age=31536000',
                'GrantRead' => "uri=http://acs.amazonaws.com/groups/global/AllUsers",
            ]);

        } catch (Exception $e) {
            var_dump($e->__toString());
            exit(0);
        }
    }

    function copyFile($source, $destination) {
        $result = $this->connection->copyObject([
            'Bucket' => getenv('S3_BUCKET'),
            'Key'    => $destination,
            'CopySource' => '/' . getenv('S3_BUCKET') . '/' . $source,
            'CacheControl' => 'public, max-age=31536000',
            'GrantRead' => "uri=http://acs.amazonaws.com/groups/global/AllUsers",
        ]);
    }

}
