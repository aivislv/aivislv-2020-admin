<?php
namespace app\helpers;

use Yii;
use yii\bootstrap4\Breadcrumbs;

class BreadcrumbsHelper {
    private $breadcrumbs = [];

    public function __construct()
    {
        $this->add('Dashboard', '/');
    }

    public function add($title, $link) {
        if ($title !== 'index') {
            $this->breadcrumbs[] = [
                'label' => ucfirst($title),
                'url' => $link,
            ];
        }
    }

    public function render() {

        $this->breadcrumbs[count($this->breadcrumbs)-1]['template'] = '<li class="active breadcrumb-item" aria-current="page"><i>' . $this->breadcrumbs[count($this->breadcrumbs)-1]['label'] . '</i></li>';

        return Breadcrumbs::widget([
            'itemTemplate' => "<li class=\"breadcrumb-item\"><i>{link}</i></li>", // template for all links
            'links' => $this->breadcrumbs,
            'homeLink' => false,
        ]);
    }

    public function read() {
        // var_dump($this->breadcrumbs);
    }
}
