FROM php:7.2.13-apache

WORKDIR /var/www/

RUN apt-get --allow-unauthenticated update && \
    apt-get --allow-unauthenticated upgrade -y && \
    apt-get --allow-unauthenticated install -y git
RUN apt-get install -y zip
RUN apt-get install -y libpng-dev
RUN curl -sS 'https://getcomposer.org/installer' | php
RUN mv 'composer.phar' '/usr/local/bin/composer'
# download yii2 via composer. disable installation (will be run later) as that causes fatal errors with installation
RUN composer create-project --prefer-dist --no-install yiisoft/yii2-app-basic basic
WORKDIR /var/www/basic

RUN docker-php-ext-configure mysqli --with-mysqli
RUN docker-php-ext-configure pdo_mysql --with-pdo-mysql
RUN docker-php-ext-install pdo pdo_mysql mysqli iconv gd

RUN apt-get install -y libmagickwand-dev --no-install-recommends && rm -rf /var/lib/apt/lists/
RUN pecl install imagick
RUN docker-php-ext-enable imagick

# update composer installation
COPY ./composer.json /var/www/basic/composer.json
RUN composer config --no-plugins allow-plugins.yiisoft/yii2-composer false
RUN composer update

# copy custom apache configuration with modrewrite enabled
COPY ./000-default.conf /etc/apache2/sites-available/000-default.conf

# enable modrewrite for yii2 request path management
RUN a2enmod rewrite

# RUN ln -s '/etc/apache2/mods-available/rewrite.load' '/etc/apache2/mods-enabled/rewrite.load'

EXPOSE 80
